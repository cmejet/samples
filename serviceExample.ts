import Config from 'react-native-config'
import * as helpers from '../helpers'
import configureStore from '../store'
import { login } from './authentication'

const { store } = configureStore()
import { USER_LOGIN_SUCCESS } from '../actions/types'

export const fetchWrapper = (api: 'PROXY' | 'MASTERCARD', endPoint: string, method = 'get', body: any = {}, headers: any = { 'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': `Bearer ${helpers.getTokens()}` }) => {
  let data: {}
  let uri
  body = JSON.stringify(body)
  if (method === 'get' || method === 'GET') {
    data = {
      method,
      headers
    }
  } else {
    data = {
      body,
      method,
      headers
    }
  }
  if (api === 'PROXY') {
    uri = `${healthPassProxy.host}${endPoint}`
  }

  if (api === 'MASTERCARD') {
    uri = `${masterCardGateway.host}${endPoint}`
  }
  console.log(`****** INITIATING HTTP REQUEST ${uri} ${JSON.stringify(data)}`)

  return fetch(uri, data)
    .then(async (response) => {
      if (jwtExpired(response) && shouldReFetchOnJwtExpire(endPoint)){
        const { email, password } = helpers.getUserCredentials()
        const sessionRes = await login({email, password})
        await store.dispatch({ type: USER_LOGIN_SUCCESS, payload: {...sessionRes.details, email, password}})
        const authToken = `Bearer ${helpers.getTokens()}`
        data.headers.Authorization = authToken
        response = await fetch(uri, data)
      }
      return response.json()
    })
    .then((json) => {
      return new Promise((resolve, reject) => {
        resolve(json)
      })
    })
    .catch(e => {
      return new Promise((resolve, reject) => {
        reject(e)
      })
    })
}

// hosts
export const healthPassProxy = {
  host: Config.PROXY_URL
}

export const masterCardGateway = {
  host: Config.MASTERCARD_GATEWAY,
  root: '/api/rest/version/48/merchant'
}

export const shouldReFetchOnJwtExpire = (endPoint: string) => {
  return endPoint.indexOf('session') === -1 && endPoint.indexOf('user') === -1
}

export const jwtExpired = (response: any) => {
  return response.status === 401 || response.message === 'jwt expired'
}