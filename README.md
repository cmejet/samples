# samples

*  **DocTracker** is an application that was completed with Angular.io, a User Interface created to interact with an application that was overly complicated, and to help manage workflow that was not enforced by the original application. This was done as a quick POC and then turned into the System of Record for the organization with plans to expand further.

*  **serviceExample.ts** is a sample of a wrapper of the react-native fetch api that allowed us common functionality to interact with our api's

*  **A11Y Testing with Protractor** a presentation I gave to my team after attenting an accessibility hack-a-thon. Sharing key learnings.

*  **ChemPlan Bad Code Smells** a presentation I put together as part of a regular session our team would do for continous learning and improvement

*  **Fighting Fires and Saving Lives** abstracts submitted to Software conference at Intel
