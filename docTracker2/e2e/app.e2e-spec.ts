import { DocTracker2Page } from './app.po';

describe('doc-tracker2 App', () => {
  let page: DocTracker2Page;

  beforeEach(() => {
    page = new DocTracker2Page();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
