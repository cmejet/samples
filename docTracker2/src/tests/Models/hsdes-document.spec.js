System.register(["../../app/models/hsdes-document.model"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var hsdes_document_model_1;
    return {
        setters: [
            function (hsdes_document_model_1_1) {
                hsdes_document_model_1 = hsdes_document_model_1_1;
            }
        ],
        execute: function () {
            describe('HsdEsDocumentShould', function () {
                describe('ParseUrl', function () {
                    var tag;
                    it('Returns Empty String When Empty Tag', function () {
                        expect(hsdes_document_model_1.HsdEsDocument.parseUrl("")).toBe("");
                    });
                    it('Returns Empty String When No Href Found In Tag', function () {
                        tag = '<?xml version="1.0" ?><dict><a  target="_blank">https://sharepoint.amr.ith.intel.com/sites/tgldoc/tglhas/Shared%20Documents/Address%20Map/TGL%20Address%20Map%20Delta%20HAS%20wip.docx</a></dict>';
                        expect(hsdes_document_model_1.HsdEsDocument.parseUrl(tag)).toBe("");
                    });
                    it('Returns valid href string When Href Found In Tag', function () {
                        tag = '<?xml version="1.0" ?><dict><a href="https://sharepoint.amr.ith.intel.com/sites/tgldoc/tglhas/Shared%20Documents/Address%20Map/TGL%20Address%20Map%20Delta%20HAS%20wip.docx" target="_blank">https://sharepoint.amr.ith.intel.com/sites/tgldoc/tglhas/Shared%20Documents/Address%20Map/TGL%20Address%20Map%20Delta%20HAS%20wip.docx</a></dict>';
                        expect(hsdes_document_model_1.HsdEsDocument.parseUrl(tag)).toBe("https://sharepoint.amr.ith.intel.com/sites/tgldoc/tglhas/Shared%20Documents/Address%20Map/TGL%20Address%20Map%20Delta%20HAS%20wip.docx");
                        expect(hsdes_document_model_1.HsdEsDocument.parseUrl('<a href="localhost:3000/query" target="_blank">DRAFT URL</a>')).toBe("localhost:3000/query");
                    });
                });
                describe('isClosed', function () {
                    var doc;
                    beforeEach(function () {
                        doc = new hsdes_document_model_1.HsdEsDocument();
                    });
                    it('returns true when status release', function () {
                        doc.status = 'release';
                        expect(doc.isClosed).toBeTruthy();
                    });
                    it('returns true when status rejected', function () {
                        doc.status = 'rejected';
                        expect(doc.isClosed).toBeTruthy();
                    });
                    it('returns true when status waived', function () {
                        doc.status = 'waived';
                        expect(doc.isClosed).toBeTruthy();
                    });
                    it('returns false when status not_started', function () {
                        doc.status = 'not_started';
                        expect(doc.isClosed).toBeFalsy();
                    });
                    it('returns false when status authoring_wip', function () {
                        doc.status = 'authoring_wip';
                        expect(doc.isClosed).toBeFalsy();
                    });
                    it('returns false when status reviewpdf_queue', function () {
                        doc.status = 'reviewpdf_queue';
                        expect(doc.isClosed).toBeFalsy();
                    });
                    it('returns false when status review_candidate', function () {
                        doc.status = 'review_candidate';
                        expect(doc.isClosed).toBeFalsy();
                    });
                    it('returns false when status review_complete', function () {
                        doc.status = 'review_complete';
                        expect(doc.isClosed).toBeFalsy();
                    });
                    it('returns false when status releasepdf_queue', function () {
                        doc.status = 'releasepdf_queue';
                        expect(doc.isClosed).toBeFalsy();
                    });
                    it('returns false when status release_candidate', function () {
                        doc.status = 'release_candidate';
                        expect(doc.isClosed).toBeFalsy();
                    });
                });
                describe('newIsValid ', function () {
                    describe('Validate new HAS form ', function () {
                        var doc = new hsdes_document_model_1.HsdEsDocument;
                        beforeEach(function () {
                        });
                        it('when mandatory fields are empty', function () {
                            doc.docName = "";
                            doc.release = "";
                            doc.releaseAffected = "";
                            doc.domain = "";
                            doc.owner = "";
                            doc.docType = "";
                            doc.ownerArchlead = "";
                            doc.designContact = "";
                            doc.techCommSupport = "";
                            doc.validationContact = "";
                            expect(doc.newIsValid).toBeFalsy();
                        });
                        it('when mandatory fields are not empty', function () {
                            doc.docName = "Test";
                            doc.release = "";
                            doc.releaseAffected = "";
                            doc.domain = "";
                            doc.owner = "rpradhan";
                            doc.docType = "test";
                            doc.ownerArchlead = "rpradhan";
                            doc.designContact = "";
                            doc.techCommSupport = "";
                            doc.validationContact = "";
                            expect(doc.newIsValid).toBeTruthy();
                        });
                    });
                });
                describe('Status updates', function () {
                    var doc = new hsdes_document_model_1.HsdEsDocument;
                    beforeEach(function () {
                        doc = new hsdes_document_model_1.HsdEsDocument();
                    });
                    describe('Validate Authoring Wip', function () {
                        beforeEach(function () {
                            doc.status = "authoring_wip";
                        });
                        it('return false when input field is empty', function () {
                            expect(doc.validateAuthoringWip).toBeFalsy();
                            expect(doc.isStatusUpdateValid).toBeFalsy();
                        });
                        it('return false when date field is empty', function () {
                            doc.draftUrl = "";
                            expect(doc.validateAuthoringWip).toBeFalsy();
                            expect(doc.isStatusUpdateValid).toBeFalsy();
                        });
                        it('return false when draft Url field is empty', function () {
                            doc.authoringEta = new Date("11/29/1997");
                            expect(doc.validateAuthoringWip).toBeFalsy();
                            expect(doc.isStatusUpdateValid).toBeFalsy();
                        });
                        it('return true when input field is populated', function () {
                            doc.draftUrl = 'www.google.com';
                            doc.authoringEta = new Date("11/29/1997");
                            expect(doc.validateAuthoringWip).toBeTruthy();
                            expect(doc.isStatusUpdateValid).toBeTruthy();
                        });
                        it('Return false when date is empty', function () {
                            doc.draftUrl = 'www.google.com';
                            doc.authoringEta = new Date("mm/dd/yyyy");
                            expect(doc.validateAuthoringWip).toBeFalsy();
                            expect(doc.isStatusUpdateValid).toBeFalsy();
                        });
                    });
                    describe('Validate Review Candidate', function () {
                        beforeEach(function () {
                            doc.status = "review_candidate";
                        });
                        it('return false when input field is empty', function () {
                            expect(doc.validateReviewCandidate).toBeFalsy();
                            expect(doc.isStatusUpdateValid).toBeFalsy();
                        });
                        it('return false when date field is empty', function () {
                            doc.reviewUrl = "";
                            expect(doc.validateReviewCandidate).toBeFalsy();
                            expect(doc.isStatusUpdateValid).toBeFalsy();
                        });
                        it('return false when draft Url field is empty', function () {
                            doc.reviewDate = new Date("11/29/1997");
                            expect(doc.validateReviewCandidate).toBeFalsy();
                            expect(doc.isStatusUpdateValid).toBeFalsy();
                        });
                        it('return true when input field is populated', function () {
                            doc.reviewUrl = 'www.google.com';
                            doc.reviewDate = new Date("11/29/1997");
                            expect(doc.validateReviewCandidate).toBeTruthy();
                            expect(doc.isStatusUpdateValid).toBeTruthy();
                        });
                    });
                    describe('Validate Release Candidate', function () {
                        beforeEach(function () {
                            doc.status = "release_candidate";
                        });
                        it('return false when input field is empty', function () {
                            expect(doc.validateReleaseCandidate).toBeFalsy();
                            expect(doc.isStatusUpdateValid).toBeFalsy();
                        });
                        it('return false when date field is empty', function () {
                            doc.releaseUrl = "";
                            expect(doc.validateReleaseCandidate).toBeFalsy();
                            expect(doc.isStatusUpdateValid).toBeFalsy();
                        });
                        it('return false when draft Url field is empty', function () {
                            doc.releaseEta = new Date("11/29/1997");
                            expect(doc.validateReleaseCandidate).toBeFalsy();
                            expect(doc.isStatusUpdateValid).toBeFalsy();
                        });
                        it('return true when input field is populated', function () {
                            doc.releaseUrl = 'www.google.com';
                            doc.releaseEta = new Date("11/29/1997");
                            expect(doc.validateReleaseCandidate).toBeTruthy();
                            expect(doc.isStatusUpdateValid).toBeTruthy();
                        });
                    });
                    describe('Validate Release', function () {
                        beforeEach(function () {
                            doc.status = "release";
                        });
                        it('return false when input field is empty', function () {
                            expect(doc.validateRelease).toBeFalsy();
                        });
                        it('return false when date field is empty', function () {
                            doc.releaseUrl = "";
                            expect(doc.validateRelease).toBeFalsy();
                        });
                    });
                    describe('Validate review complete', function () {
                        beforeEach(function () {
                            doc.status = "review_complete";
                        });
                        it('return false when input field is empty', function () {
                            expect(doc.validateRelease).toBeFalsy();
                        });
                        it('return false when date field is empty', function () {
                            doc.releaseUrl = "";
                            expect(doc.validateRelease).toBeFalsy();
                        });
                    });
                });
            });
        }
    };
});
//# sourceMappingURL=hsdes-document.spec.js.map