System.register(["../../app/models/hsdes-parentdoc.model"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var hsdes_parentdoc_model_1;
    return {
        setters: [
            function (hsdes_parentdoc_model_1_1) {
                hsdes_parentdoc_model_1 = hsdes_parentdoc_model_1_1;
            }
        ],
        execute: function () {
            describe('Parent Document Model Should', function () {
                describe('revsToWaive', function () {
                    var parent;
                    beforeEach(function () {
                        parent = new hsdes_parentdoc_model_1.HsdEsParentDoc();
                    });
                    it('returns correct list of 1', function () {
                        parent.currentRevision = "0.2";
                        parent.requestedRevision = "0.5";
                        expect(parent.revsToWaive.length).toBe(1);
                        expect(parent.revsToWaive[0].revision).toBe("0.2");
                    });
                    it('returns correct list of 2', function () {
                        parent.docs[0].status = "waived";
                        parent.currentRevision = "0.5";
                        parent.requestedRevision = "1.0";
                        expect(parent.revsToWaive.length).toBe(2);
                        expect(parent.revsToWaive[0].revision).toBe("0.5");
                        expect(parent.revsToWaive[1].revision).toBe("0.8");
                    });
                });
            });
        }
    };
});
//# sourceMappingURL=hsdes-parentdoc.spec.js.map