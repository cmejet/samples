import { HsdEsDocument } from '../../app/models/hsdes-document.model';

describe('HsdEsDocumentShould', () => {

    describe('ParseUrl', () => {

        var tag: string;

        it('Returns Empty String When Empty Tag', () => {
            expect(HsdEsDocument.parseUrl("")).toBe("");
        });

        it('Returns Empty String When No Href Found In Tag', () => {
            tag = '<?xml version="1.0" ?><dict><a  target="_blank">https://sharepoint.amr.ith.intel.com/sites/tgldoc/tglhas/Shared%20Documents/Address%20Map/TGL%20Address%20Map%20Delta%20HAS%20wip.docx</a></dict>';
            expect(HsdEsDocument.parseUrl(tag)).toBe("");
        });

        it('Returns valid href string When Href Found In Tag', () => {
            tag = '<?xml version="1.0" ?><dict><a href="https://sharepoint.amr.ith.intel.com/sites/tgldoc/tglhas/Shared%20Documents/Address%20Map/TGL%20Address%20Map%20Delta%20HAS%20wip.docx" target="_blank">https://sharepoint.amr.ith.intel.com/sites/tgldoc/tglhas/Shared%20Documents/Address%20Map/TGL%20Address%20Map%20Delta%20HAS%20wip.docx</a></dict>';
            expect(HsdEsDocument.parseUrl(tag)).toBe("https://sharepoint.amr.ith.intel.com/sites/tgldoc/tglhas/Shared%20Documents/Address%20Map/TGL%20Address%20Map%20Delta%20HAS%20wip.docx");
            expect(HsdEsDocument.parseUrl('<a href="localhost:3000/query" target="_blank">DRAFT URL</a>')).toBe("localhost:3000/query");
        });
    });


    describe('isClosed', () => {
        var doc;
        beforeEach(() => {
            doc = new HsdEsDocument();
        });

        it('returns true when status release', () => {
            doc.status = 'release';
            expect(doc.isClosed).toBeTruthy();
        });
        it('returns true when status rejected', () => {
            doc.status = 'rejected';
            expect(doc.isClosed).toBeTruthy();
        });
        it('returns true when status waived', () => {
            doc.status = 'waived';
            expect(doc.isClosed).toBeTruthy();
        });
        it('returns false when status not_started', () => {
            doc.status = 'not_started';
            expect(doc.isClosed).toBeFalsy();
        });
        it('returns false when status authoring_wip', () => {
            doc.status = 'authoring_wip';
            expect(doc.isClosed).toBeFalsy();
        });
        it('returns false when status reviewpdf_queue', () => {
            doc.status = 'reviewpdf_queue';
            expect(doc.isClosed).toBeFalsy();
        });
        it('returns false when status review_candidate', () => {
            doc.status = 'review_candidate';
            expect(doc.isClosed).toBeFalsy();
        });
        it('returns false when status review_complete', () => {
            doc.status = 'review_complete';
            expect(doc.isClosed).toBeFalsy();
        });
        it('returns false when status releasepdf_queue', () => {
            doc.status = 'releasepdf_queue';
            expect(doc.isClosed).toBeFalsy();
        });
        it('returns false when status release_candidate', () => {
            doc.status = 'release_candidate';
            expect(doc.isClosed).toBeFalsy();
        });
    });

    describe('newIsValid ', () => {
        describe('Validate new HAS form ', () => {
            var doc = new HsdEsDocument;
            beforeEach(() => {



            });
            it('when mandatory fields are empty', () => {
                doc.docName = "";
                doc.release = "";
                doc.releaseAffected = "";
                doc.domain = "";
                doc.owner = "";
                doc.docType = "";
                doc.ownerArchlead = "";
                doc.designContact = "";
                doc.techCommSupport = "";
                doc.validationContact = "";

                expect(doc.newIsValid).toBeFalsy();

            });
            it('when mandatory fields are not empty', () => {
                doc.docName = "Test";
                doc.release = "";
                doc.releaseAffected = "";
                doc.domain = "";
                doc.owner = "rpradhan";
                doc.docType = "test";
                doc.ownerArchlead = "rpradhan";
                doc.designContact = "";
                doc.techCommSupport = "";
                doc.validationContact = "";
                expect(doc.newIsValid).toBeTruthy();

            });


        });
    });

    describe('Status updates', () => {
        var doc = new HsdEsDocument;
        beforeEach(() => {
            doc = new HsdEsDocument();
        })
        describe('Validate Authoring Wip', () => {
            beforeEach(() => {
                doc.status = "authoring_wip";
            })

            it('return false when input field is empty', () => {
                expect(doc.validateAuthoringWip).toBeFalsy();
                expect(doc.isStatusUpdateValid).toBeFalsy();
            });

            it('return false when date field is empty', () => {
                doc.draftUrl = "";
                expect(doc.validateAuthoringWip).toBeFalsy();
                expect(doc.isStatusUpdateValid).toBeFalsy();
            });

            it('return false when draft Url field is empty', () => {
                doc.authoringEta = new Date("11/29/1997");
                expect(doc.validateAuthoringWip).toBeFalsy();
                expect(doc.isStatusUpdateValid).toBeFalsy();

            });

            it('return true when input field is populated', () => {
                doc.draftUrl = 'www.google.com';
                doc.authoringEta = new Date("11/29/1997");
                expect(doc.validateAuthoringWip).toBeTruthy();
                expect(doc.isStatusUpdateValid).toBeTruthy();
            });

            it('Return false when date is empty', () => {
                doc.draftUrl = 'www.google.com';
                doc.authoringEta = new Date("mm/dd/yyyy");
                expect(doc.validateAuthoringWip).toBeFalsy();
                expect(doc.isStatusUpdateValid).toBeFalsy();
            });
        });


        describe('Validate Review Candidate', () => {
            beforeEach(() => {
                doc.status = "review_candidate";
            })

            it('return false when input field is empty', () => {
                expect(doc.validateReviewCandidate).toBeFalsy();
                expect(doc.isStatusUpdateValid).toBeFalsy();
            });

            it('return false when date field is empty', () => {
                doc.reviewUrl = "";
                expect(doc.validateReviewCandidate).toBeFalsy();
                expect(doc.isStatusUpdateValid).toBeFalsy();
            });

            it('return false when draft Url field is empty', () => {
                doc.reviewDate = new Date("11/29/1997");
                expect(doc.validateReviewCandidate).toBeFalsy();
                expect(doc.isStatusUpdateValid).toBeFalsy();
            });


            it('return true when input field is populated', () => {
                doc.reviewUrl = 'www.google.com';
                doc.reviewDate = new Date("11/29/1997");
                expect(doc.validateReviewCandidate).toBeTruthy();
                expect(doc.isStatusUpdateValid).toBeTruthy();
            });
        });


        describe('Validate Release Candidate', () => {
            beforeEach(() => {
                doc.status = "release_candidate";
            })

            it('return false when input field is empty', () => {
                expect(doc.validateReleaseCandidate).toBeFalsy();
                expect(doc.isStatusUpdateValid).toBeFalsy();
            });

            it('return false when date field is empty', () => {
                doc.releaseUrl = "";
                expect(doc.validateReleaseCandidate).toBeFalsy();
                expect(doc.isStatusUpdateValid).toBeFalsy();
            });

            it('return false when draft Url field is empty', () => {
                doc.releaseEta = new Date("11/29/1997");
                expect(doc.validateReleaseCandidate).toBeFalsy();
                expect(doc.isStatusUpdateValid).toBeFalsy();
            });


            it('return true when input field is populated', () => {
                doc.releaseUrl = 'www.google.com';
                doc.releaseEta = new Date("11/29/1997");
                expect(doc.validateReleaseCandidate).toBeTruthy();
                expect(doc.isStatusUpdateValid).toBeTruthy();

            });


        });
        describe('Validate Release', () => {
            beforeEach(() => {
                doc.status = "release";
            })

            it('return false when input field is empty', () => {
                expect(doc.validateRelease).toBeFalsy();
            });

            it('return false when date field is empty', () => {
                doc.releaseUrl = "";
                expect(doc.validateRelease).toBeFalsy();
            });

        });
        describe('Validate review complete', () => {
            beforeEach(() => {
                doc.status = "review_complete";
            })

            it('return false when input field is empty', () => {
                expect(doc.validateRelease).toBeFalsy();
            });

            it('return false when date field is empty', () => {
                doc.releaseUrl = "";
                expect(doc.validateRelease).toBeFalsy();
            });

        });

    });

});