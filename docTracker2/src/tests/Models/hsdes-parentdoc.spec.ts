import { HsdEsParentDoc } from '../../app/models/hsdes-parentdoc.model';
describe('Parent Document Model Should', () => {
    
    describe('revsToWaive', () => {
        var parent;
        beforeEach(() => {
            parent = new HsdEsParentDoc();
        });
            
        it('returns correct list of 1', () => {
            parent.currentRevision = "0.2";
            parent.requestedRevision = "0.5";
            expect(parent.revsToWaive.length).toBe(1);
            expect(parent.revsToWaive[0].revision).toBe("0.2");
        });
            
        it('returns correct list of 2', () => {
            parent.docs[0].status = "waived";
            parent.currentRevision = "0.5";
            parent.requestedRevision = "1.0";
            expect(parent.revsToWaive.length).toBe(2);
            expect(parent.revsToWaive[0].revision).toBe("0.5");
            expect(parent.revsToWaive[1].revision).toBe("0.8");
        });
            
    });
        
});
    