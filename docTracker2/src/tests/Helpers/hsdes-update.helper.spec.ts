import { HsdEsRequest } from '../../app/models/hsdes-request.model';
import { HsdEsDocument } from '../../app/models/hsdes-document.model';
import { HsdEsUpdateHelper } from '../../app/shared/helpers/hsdes-update.helper';

var convertedGoogleUrlDraft = `<?xml version="1.0" ?><dict><a href="http://google.com" target="_blank">Draft URL</a></dict>`;
var convertedGoogleUrlRelease = `<?xml version="1.0" ?><dict><a href="http://google.com" target="_blank">Release URL</a></dict>`;
var convertedGoogleUrlReview = `<?xml version="1.0" ?><dict><a href="http://google.com" target="_blank">Review URL</a></dict>`;
var googleUrl = "http://google.com";
describe('HsdEsUpdate Helper Should', () => {
    var doc;
    beforeEach(() => {
        doc = new HsdEsDocument();
        doc.id = "123456789";
        doc.tenant = "phablet";
    });
    describe('Create Request Object for URL Update', () => {
        it('return completed request for URL update', () => {
            doc.id;
            doc.releaseUrl = googleUrl;
            doc.reviewUrl = "something@intel.com";
            doc.draftUrl = "something@yahoo.com";
            var request = HsdEsUpdateHelper.createUrlRequestObject(doc);
            expect(request.command).toBe("update_record_with_fetch");
            expect(request.command_args.id).toBe("123456789");
            expect(request.command_args.subject).toBe("doc_tracking");
            expect(request.var_args[0]["doc_tracking.release_url"]).toBe(convertedGoogleUrlRelease);
            expect(request.var_args[1]["doc_tracking.review_url"]).toBe(`<?xml version="1.0" ?><dict><a href="something@intel.com" target="_blank">Review URL</a></dict>`);
            expect(request.var_args[2]["doc_tracking.draft_url"]).toBe(`<?xml version="1.0" ?><dict><a href="something@yahoo.com" target="_blank">Draft URL</a></dict>`);
        });
    });

    describe('Create Request Object for Status Update', () => {
        it('return completed request for status of authoring_wip', () => {
            doc.status = "authoring_wip";
            doc.draftUrl = googleUrl;
            doc.authoringEta = "2017-06-09 00:00:00";
            var request = HsdEsUpdateHelper.createStatusRequestObject(doc);
            expect(request.command).toBe("update_record_with_fetch");
            expect(request.command_args.id).toBe(doc.id);
            expect(request.command_args.subject).toBe("doc_tracking");
            expect(request.command_args.tenant).toBe("phablet");
            expect(request.var_args[0].status).toBe(doc.status);
            expect(request.var_args[1]["doc_tracking.draft_url"]).toBe(convertedGoogleUrlDraft);
            expect(request.var_args[2]["doc_tracking.authoring_eta"]).toBe(doc.authoringEta);
        });

        it('return request for status of reviewpdf_queue', () => {
            doc.status = "reviewpdf_queue";
            doc.draftUrl = googleUrl;
            var request = HsdEsUpdateHelper.createStatusRequestObject(doc);
            expect(request.var_args[0].status).toBe(doc.status);
            expect(request.var_args[1]["doc_tracking.draft_url"]).toBe(convertedGoogleUrlDraft);
        });

        it('return request for status of review_candidate', () => {
            doc.status = "review_candidate";
            doc.reviewUrl = googleUrl;
            doc.reviewDate = "2017-06-09 00:00:00";
            var request = HsdEsUpdateHelper.createStatusRequestObject(doc);
            expect(request.var_args[0].status).toBe(doc.status);
            expect(request.var_args[1]["doc_tracking.review_url"]).toBe(convertedGoogleUrlReview);
            expect(request.var_args[2]["doc_tracking.review_date"]).toBe(doc.reviewDate);
        });

        it('return request for status of review_complete', () => {
            doc.status = "review_complete";
            doc.reviewDate = "2017-06-09 00:00:00";
            doc.releaseEta = "2017-06-10 00:00:00";
            var request = HsdEsUpdateHelper.createStatusRequestObject(doc);
            expect(request.var_args[0].status).toBe(doc.status);
            expect(request.var_args[1]["doc_tracking.review_date"]).toBe(doc.reviewDate);
            expect(request.var_args[2]["doc_tracking.release_eta"]).toBe(doc.releaseEta);
        });
        it('return request for status of releasepdf_queue', () => {
            doc.status = "releasepdf_queue";
            var request = HsdEsUpdateHelper.createStatusRequestObject(doc);
            expect(request.var_args[0].status).toBe(doc.status);
            expect(request.var_args.length).toBe(1);
        });
        it('return request for status of release_candidate', () => {
            doc.status = "release_candidate";
            doc.releaseUrl = googleUrl;
            var request = HsdEsUpdateHelper.createStatusRequestObject(doc);
            expect(request.var_args[0].status).toBe(doc.status);
            expect(request.var_args[1]["doc_tracking.release_eta"]).toBe(doc.releaseEta);
            expect(request.var_args[2]["doc_tracking.release_url"]).toBe(convertedGoogleUrlRelease);
        });
        it('return request for status of release', () => {
            doc.status = "release";
            doc.releaseUrl = googleUrl;
            var request = HsdEsUpdateHelper.createStatusRequestObject(doc);
            expect(request.var_args[0].status).toBe(doc.status);
            expect(request.var_args[1]["doc_tracking.release_url"]).toBe(convertedGoogleUrlRelease);
        });


    });


});
