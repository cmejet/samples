import { HsdEsRequest } from '../../app/models/hsdes-request.model';
import { HsdEsDocument } from '../../app/models/hsdes-document.model';
import { HsdEsInsertHelper } from '../../app/shared/helpers/hsdes-insert.helper';

describe('HsdEsInsert Helper Should', () => {
    describe('Insert New Has', () => {
        var doc;
        beforeEach(() => {
            doc = new HsdEsDocument();

            doc.docName = "DocTracker Testing J&R";
            doc.release = "tah-a0";
            doc.releaseAffected = "";
            doc.domain = "audio";
            doc.owner = "jlrobiso";
            doc.docType = "soc.has";
            doc.ownerArchlead = "jlrobiso";
            doc.designContact = "rpradhan";
            doc.validationContact = "";
            doc.techCommSupport = "";

        });

        it('parent return completed request for insert', () => {

            doc.status = "";
            doc.currentRevision = "";
            doc.parentId = "";
            doc.sendMail = false;


            var request = HsdEsInsertHelper.insertParentRecord(doc);
            expect(request.command).toBe("insert_record_with_fetch");
            expect(request.var_args[0]["title"]).toBe(doc.docName);
            expect(request.var_args[1]["release"]).toBe(doc.release);
            expect(request.var_args[2]["release_affected"]).toBe(doc.releaseAffected);
            expect(request.var_args[3]["domain"]).toBe(doc.domain);
            expect(request.var_args[4]["owner"]).toBe(doc.owner);
            expect(request.var_args[5]["status"]).toBe(doc.status);
            expect(request.var_args[6]["doc_tracking.doc_type"]).toBe(doc.docType);
            expect(request.var_args[7]["doc_tracking.rev_current"]).toBe(doc.currentRevision);
            expect(request.var_args[8]["doc_tracking.owner_archlead"]).toBe(doc.ownerArchlead);
            expect(request.var_args[9]["doc_tracking.design_contact"]).toBe(doc.designContact);
            expect(request.var_args[10]["doc_tracking.validation_contact"]).toBe(doc.validationContact);
            expect(request.var_args[11]["doc_tracking.tech_comm_support"]).toBe(doc.techCommSupport);
            expect(request.var_args[12]["send_mail"]).toBe(doc.sendMail);
           // expect(request.var_args[13]["doc_tracking.level"]).toBe("L1");
            //expect(request.var_args[13]["parent_id"]).toBe(doc.parentId);

        });


        it('child return completed request for insert', () => {

            doc.status = "not_started";
            doc.currentRevision = "";
           // doc.parentId = "1304363612";
            doc.sendMail = true;


            var request = HsdEsInsertHelper.insertChildRecord(doc);
            expect(request.command).toBe("insert_record_with_fetch");
            expect(request.var_args[0]["title"]).toBe(doc.docName);
            expect(request.var_args[1]["release"]).toBe(doc.release);
            expect(request.var_args[2]["release_affected"]).toBe(doc.releaseAffected);
            expect(request.var_args[3]["domain"]).toBe(doc.domain);
            expect(request.var_args[4]["owner"]).toBe(doc.owner);
            expect(request.var_args[5]["status"]).toBe(doc.status);
            expect(request.var_args[6]["doc_tracking.doc_type"]).toBe(doc.docType);
            expect(request.var_args[7]["doc_tracking.rev_current"]).toBe(doc.currentRevision);
            expect(request.var_args[8]["doc_tracking.owner_archlead"]).toBe(doc.ownerArchlead);
            expect(request.var_args[9]["doc_tracking.design_contact"]).toBe(doc.designContact);
            expect(request.var_args[10]["doc_tracking.validation_contact"]).toBe(doc.validationContact);
            expect(request.var_args[11]["doc_tracking.tech_comm_support"]).toBe(doc.techCommSupport);
            expect(request.var_args[12]["send_mail"]).toBe(doc.sendMail);
            //expect(request.var_args[13]["doc_tracking.level"]).toBe("L2");
            //expect(request.var_args[13]["parent_id"]).toBe(doc.parentId);

        });

    });
});