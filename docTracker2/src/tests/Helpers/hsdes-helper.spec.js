System.register(["../../app/shared/helpers/hsdes-helper", "../../app/models/hsdes-document.model", "../../app/models/MockData/hsdes-mock"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var hsdes_helper_1, hsdes_document_model_1, hsdes_mock_1;
    return {
        setters: [
            function (hsdes_helper_1_1) {
                hsdes_helper_1 = hsdes_helper_1_1;
            },
            function (hsdes_document_model_1_1) {
                hsdes_document_model_1 = hsdes_document_model_1_1;
            },
            function (hsdes_mock_1_1) {
                hsdes_mock_1 = hsdes_mock_1_1;
            }
        ],
        execute: function () {
            describe('HsdEsHelper Should', function () {
                describe('CreateHierarchy', function () {
                    var mockData;
                    beforeEach(function () {
                        mockData = hsdes_mock_1.HsdEsMockData.TigerLakeDocs();
                    });
                    it('Creates Array of ParentDocs', function () {
                        var parentDocs = hsdes_helper_1.HsdEsHelper.createHierarchy(mockData);
                        expect(parentDocs.length).toBe(22);
                        expect(parentDocs.every(function (p) { return p.docs.length == 5; })).toBeTruthy();
                    });
                });
                describe('GetCurrentRevision', function () {
                    var docs;
                    var revisions;
                    beforeEach(function () {
                        revisions = hsdes_helper_1.HsdEsHelper.Revisions;
                        docs = [];
                        revisions.forEach(function (r) {
                            var doc = new hsdes_document_model_1.HsdEsDocument();
                            doc.revision = r;
                            doc.status = "not_started";
                            docs.push(doc);
                        });
                    });
                    it('returns default value of revision if no completed revision', function () {
                        expect(hsdes_helper_1.HsdEsHelper.getCurrentRevision(docs)).toBe("0.2");
                    });
                    it('returns 0.5 if 0.2 is waived', function () {
                        docs.find(function (d) { return d.revision == "0.2"; }).status = "waived";
                        expect(hsdes_helper_1.HsdEsHelper.getCurrentRevision(docs)).toBe("0.5");
                    });
                    it('returns 0.5 if 0.2 is released', function () {
                        docs.find(function (d) { return d.revision == "0.2"; }).status = "release";
                        expect(hsdes_helper_1.HsdEsHelper.getCurrentRevision(docs)).toBe("0.5");
                    });
                    it('returns 1.1 when current release is 1.1', function () {
                        docs.forEach(function (d) { return d.status = "waived"; });
                        docs.find(function (d) { return d.revision == "1.1"; }).status == "release";
                        expect(hsdes_helper_1.HsdEsHelper.getCurrentRevision(docs)).toBe("1.1");
                    });
                    it('returns 1.0 when current release is 0.8', function () {
                        docs.find(function (d) { return d.revision == "0.2"; }).status = "waived";
                        docs.find(function (d) { return d.revision == "0.5"; }).status = "release";
                        docs.find(function (d) { return d.revision == "0.8"; }).status = "waived";
                        expect(hsdes_helper_1.HsdEsHelper.getCurrentRevision(docs)).toBe("1.0");
                    });
                });
                describe('Build Query Release Affected Statement', function () {
                    it('Return empty string when no Release selected', function () {
                        var releases = [];
                        expect(hsdes_helper_1.HsdEsHelper.buildQueryReleaseStatement(releases)).toBe("");
                    });
                    it('Add Contains Statements For One Release', function () {
                        var releases = ["chassis-2.0"];
                        expect(hsdes_helper_1.HsdEsHelper.buildQueryReleaseStatement(releases)).toBe(" OR release_affected contains 'chassis-2.0'");
                    });
                    it('Add Contains Statements For Each Release', function () {
                        var releases = ["chassis-2.0", "chassis-future", "tgl-u-a0"];
                        expect(hsdes_helper_1.HsdEsHelper.buildQueryReleaseStatement(releases)).toBe(" OR release_affected contains 'chassis-2.0' OR release_affected contains 'chassis-future' OR release_affected contains 'tgl-u-a0'");
                    });
                });
                describe('Build Release Statement', function () {
                    it('Return empty string when no Release selected', function () {
                        var releases = [];
                        expect(hsdes_helper_1.HsdEsHelper.buildReleaseStatement(releases)).toBe("");
                    });
                    it('Add Release For One Release', function () {
                        var releases = ["chassis-future"];
                        expect(hsdes_helper_1.HsdEsHelper.buildReleaseStatement(releases)).toBe(" and release in ('chassis-future')");
                    });
                    it('Add Release For Two Release', function () {
                        var releases = ["chassis-2.0", "chassis-future", "tgl-u-a0"];
                        expect(hsdes_helper_1.HsdEsHelper.buildReleaseStatement(releases)).toBe(" and release in ('chassis-2.0','chassis-future','tgl-u-a0')");
                    });
                });
                describe('AvailableStatusBasedOnCurrentStatus', function () {
                    it('Return default status if no status passed in', function () {
                        var statuses = hsdes_helper_1.HsdEsHelper.availableStatusBasedOnCurrentStatus();
                        expect(statuses.length).toBe(1);
                        expect(statuses[0]).toBe("not_started");
                    });
                    it('Return correct status for not_started', function () {
                        var statuses = hsdes_helper_1.HsdEsHelper.availableStatusBasedOnCurrentStatus('not_started');
                        expect(statuses.length).toBe(2);
                        expect(statuses[0]).toBe("not_started");
                        expect(statuses[1]).toBe("authoring_wip");
                    });
                    it('Return correct status for authoring_wip', function () {
                        var statuses = hsdes_helper_1.HsdEsHelper.availableStatusBasedOnCurrentStatus('authoring_wip');
                        expect(statuses.length).toBe(2);
                        expect(statuses[0]).toBe("authoring_wip");
                        expect(statuses[1]).toBe("reviewpdf_queue");
                    });
                    it('Return current status for waived', function () {
                        var statuses = hsdes_helper_1.HsdEsHelper.availableStatusBasedOnCurrentStatus('waived');
                        expect(statuses.length).toBe(1);
                        expect(statuses[0]).toBe("waived");
                    });
                });
            });
        }
    };
});
//# sourceMappingURL=hsdes-helper.spec.js.map