import { HsdEsHelper } from '../../app/shared/helpers/hsdes-helper';
import { HsdEsDocument } from '../../app/models/hsdes-document.model';

import { HsdEsMockData } from '../../app/models/MockData/hsdes-mock';
import { asEnumerable } from 'linq-es5';
describe('HsdEsHelper Should', () => {
    describe('CreateHierarchy', () => {
        var mockData: HsdEsDocument[];
        beforeEach(() => {
            mockData = HsdEsMockData.TigerLakeDocs();
        });

        it('Creates Array of ParentDocs', () => {
            var parentDocs = HsdEsHelper.createHierarchy(mockData);
            expect(parentDocs.length).toBe(22);
            expect(parentDocs.every(p => p.docs.length == 5)).toBeTruthy();
        });
    });

    describe('GetCurrentRevision', () => {
        var docs: HsdEsDocument[];
        var revisions: string[]
        beforeEach(() => {
            revisions = HsdEsHelper.Revisions;
            docs = [];
            revisions.forEach(r => {
                var doc = new HsdEsDocument();
                doc.revision = r;
                doc.status = "not_started"
                docs.push(doc);
            });
        });

        it('returns default value of revision if no completed revision', () => {
            expect(HsdEsHelper.getCurrentRevision(docs)).toBe("0.2");
        });
        it('returns 0.5 if 0.2 is waived', () => {
            docs.find(d => d.revision == "0.2").status = "waived";
            expect(HsdEsHelper.getCurrentRevision(docs)).toBe("0.5");
        });
        it('returns 0.5 if 0.2 is released', () => {
            docs.find(d => d.revision == "0.2").status = "release";
            expect(HsdEsHelper.getCurrentRevision(docs)).toBe("0.5");
        });

        it('returns 1.1 when current release is 1.1', () => {
            docs.forEach(d => d.status = "waived");
            docs.find(d => d.revision == "1.1").status == "release";
            expect(HsdEsHelper.getCurrentRevision(docs)).toBe("1.1");
        });
        it('returns 1.0 when current release is 0.8', () => {
            docs.find(d => d.revision == "0.2").status = "waived";
            docs.find(d => d.revision == "0.5").status = "release";
            docs.find(d => d.revision == "0.8").status = "waived";
            expect(HsdEsHelper.getCurrentRevision(docs)).toBe("1.0");
        });
    });

    describe('Build Query Release Affected Statement', () => {
        it('Return empty string when no Release selected', () => {
            var releases = [];
            expect(HsdEsHelper.buildQueryReleaseStatement(releases)).toBe("");
        });

        it('Add Contains Statements For One Release', () => {
            var releases = ["chassis-2.0"];
            expect(HsdEsHelper.buildQueryReleaseStatement(releases)).toBe(" OR release_affected contains 'chassis-2.0'");
        });

        it('Add Contains Statements For Each Release', () => {
            var releases = ["chassis-2.0", "chassis-future", "tgl-u-a0"];
            expect(HsdEsHelper.buildQueryReleaseStatement(releases)).toBe(" OR release_affected contains 'chassis-2.0' OR release_affected contains 'chassis-future' OR release_affected contains 'tgl-u-a0'");
        });



    });
    describe('Build Release Statement', () => {

        it('Return empty string when no Release selected', () => {
            var releases = [];
            expect(HsdEsHelper.buildReleaseStatement(releases)).toBe("");
        });

        it('Add Release For One Release', () => {
            var releases = ["chassis-future"];
            expect(HsdEsHelper.buildReleaseStatement(releases)).toBe(" and release in ('chassis-future')");
        });

        it('Add Release For Two Release', () => {
            var releases = ["chassis-2.0", "chassis-future", "tgl-u-a0"];
            expect(HsdEsHelper.buildReleaseStatement(releases)).toBe(" and release in ('chassis-2.0','chassis-future','tgl-u-a0')");
        });
    });

    describe('AvailableStatusBasedOnCurrentStatus', () => {

        it('Return default status if no status passed in', () => {
            var statuses = HsdEsHelper.availableStatusBasedOnCurrentStatus();
            expect(statuses.length).toBe(1);
            expect(statuses[0]).toBe("not_started");
        });

        
        it('Return correct status for not_started', () => {
            var statuses = HsdEsHelper.availableStatusBasedOnCurrentStatus('not_started');
            expect(statuses.length).toBe(2);
            expect(statuses[0]).toBe("not_started");
            expect(statuses[1]).toBe("authoring_wip");
        });

        
        it('Return correct status for authoring_wip', () => {
            var statuses = HsdEsHelper.availableStatusBasedOnCurrentStatus('authoring_wip');
            expect(statuses.length).toBe(2);
            expect(statuses[0]).toBe("authoring_wip");
            expect(statuses[1]).toBe("reviewpdf_queue");
        });
            
        
        it('Return current status for waived', () => {
            var statuses = HsdEsHelper.availableStatusBasedOnCurrentStatus('waived');
            expect(statuses.length).toBe(1);
            expect(statuses[0]).toBe("waived");
            
        });
            

    });


});


