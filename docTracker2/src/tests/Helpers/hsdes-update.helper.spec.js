System.register(["../../app/models/hsdes-document.model", "../../app/shared/helpers/hsdes-update.helper"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var hsdes_document_model_1, hsdes_update_helper_1, convertedGoogleUrlDraft, convertedGoogleUrlRelease, convertedGoogleUrlReview, googleUrl;
    return {
        setters: [
            function (hsdes_document_model_1_1) {
                hsdes_document_model_1 = hsdes_document_model_1_1;
            },
            function (hsdes_update_helper_1_1) {
                hsdes_update_helper_1 = hsdes_update_helper_1_1;
            }
        ],
        execute: function () {
            convertedGoogleUrlDraft = "<?xml version=\"1.0\" ?><dict><a href=\"http://google.com\" target=\"_blank\">Draft URL</a></dict>";
            convertedGoogleUrlRelease = "<?xml version=\"1.0\" ?><dict><a href=\"http://google.com\" target=\"_blank\">Release URL</a></dict>";
            convertedGoogleUrlReview = "<?xml version=\"1.0\" ?><dict><a href=\"http://google.com\" target=\"_blank\">Review URL</a></dict>";
            googleUrl = "http://google.com";
            describe('HsdEsUpdate Helper Should', function () {
                var doc;
                beforeEach(function () {
                    doc = new hsdes_document_model_1.HsdEsDocument();
                    doc.id = "123456789";
                    doc.tenant = "phablet";
                });
                describe('Create Request Object for URL Update', function () {
                    it('return completed request for URL update', function () {
                        doc.id;
                        doc.releaseUrl = googleUrl;
                        doc.reviewUrl = "something@intel.com";
                        doc.draftUrl = "something@yahoo.com";
                        var request = hsdes_update_helper_1.HsdEsUpdateHelper.createUrlRequestObject(doc);
                        expect(request.command).toBe("update_record_with_fetch");
                        expect(request.command_args.id).toBe("123456789");
                        expect(request.command_args.subject).toBe("doc_tracking");
                        expect(request.var_args[0]["doc_tracking.release_url"]).toBe(convertedGoogleUrlRelease);
                        expect(request.var_args[1]["doc_tracking.review_url"]).toBe("<?xml version=\"1.0\" ?><dict><a href=\"something@intel.com\" target=\"_blank\">Review URL</a></dict>");
                        expect(request.var_args[2]["doc_tracking.draft_url"]).toBe("<?xml version=\"1.0\" ?><dict><a href=\"something@yahoo.com\" target=\"_blank\">Draft URL</a></dict>");
                    });
                });
                describe('Create Request Object for Status Update', function () {
                    it('return completed request for status of authoring_wip', function () {
                        doc.status = "authoring_wip";
                        doc.draftUrl = googleUrl;
                        doc.authoringEta = "2017-06-09 00:00:00";
                        var request = hsdes_update_helper_1.HsdEsUpdateHelper.createStatusRequestObject(doc);
                        expect(request.command).toBe("update_record_with_fetch");
                        expect(request.command_args.id).toBe(doc.id);
                        expect(request.command_args.subject).toBe("doc_tracking");
                        expect(request.command_args.tenant).toBe("phablet");
                        expect(request.var_args[0].status).toBe(doc.status);
                        expect(request.var_args[1]["doc_tracking.draft_url"]).toBe(convertedGoogleUrlDraft);
                        expect(request.var_args[2]["doc_tracking.authoring_eta"]).toBe(doc.authoringEta);
                    });
                    it('return request for status of reviewpdf_queue', function () {
                        doc.status = "reviewpdf_queue";
                        doc.draftUrl = googleUrl;
                        var request = hsdes_update_helper_1.HsdEsUpdateHelper.createStatusRequestObject(doc);
                        expect(request.var_args[0].status).toBe(doc.status);
                        expect(request.var_args[1]["doc_tracking.draft_url"]).toBe(convertedGoogleUrlDraft);
                    });
                    it('return request for status of review_candidate', function () {
                        doc.status = "review_candidate";
                        doc.reviewUrl = googleUrl;
                        doc.reviewDate = "2017-06-09 00:00:00";
                        var request = hsdes_update_helper_1.HsdEsUpdateHelper.createStatusRequestObject(doc);
                        expect(request.var_args[0].status).toBe(doc.status);
                        expect(request.var_args[1]["doc_tracking.review_url"]).toBe(convertedGoogleUrlReview);
                        expect(request.var_args[2]["doc_tracking.review_date"]).toBe(doc.reviewDate);
                    });
                    it('return request for status of review_complete', function () {
                        doc.status = "review_complete";
                        doc.reviewDate = "2017-06-09 00:00:00";
                        doc.releaseEta = "2017-06-10 00:00:00";
                        var request = hsdes_update_helper_1.HsdEsUpdateHelper.createStatusRequestObject(doc);
                        expect(request.var_args[0].status).toBe(doc.status);
                        expect(request.var_args[1]["doc_tracking.review_date"]).toBe(doc.reviewDate);
                        expect(request.var_args[2]["doc_tracking.release_eta"]).toBe(doc.releaseEta);
                    });
                    it('return request for status of releasepdf_queue', function () {
                        doc.status = "releasepdf_queue";
                        var request = hsdes_update_helper_1.HsdEsUpdateHelper.createStatusRequestObject(doc);
                        expect(request.var_args[0].status).toBe(doc.status);
                        expect(request.var_args.length).toBe(1);
                    });
                    it('return request for status of release_candidate', function () {
                        doc.status = "release_candidate";
                        doc.releaseUrl = googleUrl;
                        var request = hsdes_update_helper_1.HsdEsUpdateHelper.createStatusRequestObject(doc);
                        expect(request.var_args[0].status).toBe(doc.status);
                        expect(request.var_args[1]["doc_tracking.release_eta"]).toBe(doc.releaseEta);
                        expect(request.var_args[2]["doc_tracking.release_url"]).toBe(convertedGoogleUrlRelease);
                    });
                    it('return request for status of release', function () {
                        doc.status = "release";
                        doc.releaseUrl = googleUrl;
                        var request = hsdes_update_helper_1.HsdEsUpdateHelper.createStatusRequestObject(doc);
                        expect(request.var_args[0].status).toBe(doc.status);
                        expect(request.var_args[1]["doc_tracking.release_url"]).toBe(convertedGoogleUrlRelease);
                    });
                });
            });
        }
    };
});
//# sourceMappingURL=hsdes-update.helper.spec.js.map