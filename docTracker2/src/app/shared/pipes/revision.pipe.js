System.register(["@angular/core"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, RevisionPipe;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            }
        ],
        execute: function () {
            RevisionPipe = (function () {
                function RevisionPipe() {
                }
                RevisionPipe.prototype.transform = function (items, filter) {
                    if (!items || !filter) {
                        return items;
                    }
                    // filter items array, items which match and return true will be kept, false will be filtered out
                    return items.filter(function (item) { return item.revision.indexOf(filter) !== -1; });
                };
                return RevisionPipe;
            }());
            RevisionPipe = __decorate([
                core_1.Pipe({
                    name: 'revision',
                    pure: false
                })
            ], RevisionPipe);
            exports_1("RevisionPipe", RevisionPipe);
        }
    };
});
//# sourceMappingURL=revision.pipe.js.map