System.register(["../../models/hsdes-request.model"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var hsdes_request_model_1, HsdEsUpdateHelper;
    return {
        setters: [
            function (hsdes_request_model_1_1) {
                hsdes_request_model_1 = hsdes_request_model_1_1;
            }
        ],
        execute: function () {
            HsdEsUpdateHelper = (function () {
                function HsdEsUpdateHelper() {
                }
                HsdEsUpdateHelper.createUrlRequestObject = function (doc) {
                    var request = this.createBaseRequestObject(doc);
                    request.var_args = [
                        { "doc_tracking.release_url": this.convertUrl(doc.releaseUrl, "Release") },
                        { "doc_tracking.review_url": this.convertUrl(doc.reviewUrl, "Review") },
                        { "doc_tracking.draft_url": this.convertUrl(doc.draftUrl, "Draft") }
                    ];
                    return request;
                };
                HsdEsUpdateHelper.convertUrl = function (url, type) {
                    var hsdEsUrlFormat = "<?xml version=\"1.0\" ?><dict><a href=\"" + url + "\" target=\"_blank\">" + type + " URL</a></dict>";
                    return hsdEsUrlFormat;
                };
                HsdEsUpdateHelper.createBaseRequestObject = function (doc) {
                    var request = new hsdes_request_model_1.HsdEsRequest();
                    request.command = "update_record_with_fetch";
                    request.command_args.tenant = doc.tenant;
                    request.command_args.id = doc.id;
                    request.var_args = [];
                    return request;
                };
                HsdEsUpdateHelper.createWaiveRequestObject = function (doc) {
                    var request = this.createBaseRequestObject(doc);
                    request.var_args.push({ "status": "waived" });
                    request.var_args.push({ "send_mail": false });
                    return request;
                };
                HsdEsUpdateHelper.createRejectRequestObject = function (doc) {
                    var request = this.createBaseRequestObject(doc);
                    request.var_args.push({ "status": "rejected" });
                    request.var_args.push({ "send_mail": false });
                    return request;
                };
                HsdEsUpdateHelper.createRejectParentRequestObject = function (parentdoc) {
                    var request = this.createBaseRequestObject(parentdoc.docs[0]);
                    request.command_args.id = parentdoc.id;
                    request.var_args.push({ "status": "rejected" });
                    request.var_args.push({ "send_mail": false });
                    return request;
                };
                HsdEsUpdateHelper.createStatusRequestObject = function (doc) {
                    var request = this.createBaseRequestObject(doc);
                    request.var_args.push({ "status": doc.status });
                    if (this.needsDraftUrl(doc.status))
                        request.var_args.push({ "doc_tracking.draft_url": this.convertUrl(doc.draftUrl, "Draft") });
                    if (doc.status == "authoring_wip")
                        request.var_args.push({ "doc_tracking.authoring_eta": doc.authoringEta });
                    if (doc.status == "review_candidate")
                        request.var_args.push({ "doc_tracking.review_url": this.convertUrl(doc.reviewUrl, "Review") });
                    if (doc.status.indexOf("review_") > -1)
                        request.var_args.push({ "doc_tracking.review_date": doc.reviewDate });
                    if (this.needsReleaseEta(doc.status))
                        request.var_args.push({ "doc_tracking.release_eta": doc.releaseEta });
                    if (this.needsReleaseUrl(doc.status))
                        request.var_args.push({ "doc_tracking.release_url": this.convertUrl(doc.releaseUrl, "Release") });
                    return request;
                };
                HsdEsUpdateHelper.needsDraftUrl = function (status) {
                    return status == "authoring_wip" || status == "reviewpdf_queue";
                };
                HsdEsUpdateHelper.needsReleaseEta = function (status) {
                    return status == "review_complete" || status == "release_candidate";
                };
                HsdEsUpdateHelper.needsReleaseUrl = function (status) {
                    return status == "release_candidate" || status == "release";
                };
                HsdEsUpdateHelper.editParentRecord = function (parentdoc) {
                    var request = new hsdes_request_model_1.HsdEsRequest();
                    request.command = "update_record_with_fetch";
                    request.command_args.tenant = parentdoc.selectedDoc.tenant;
                    request.command_args.id = parentdoc.id;
                    request.var_args = [
                        { "title": parentdoc.docName },
                        { "release": parentdoc.release },
                        { "release_affected": parentdoc.releaseAffected },
                        { "domain": parentdoc.domain },
                        { "owner": parentdoc.owner },
                        // { "status": parentdoc.status },
                        { "doc_tracking.doc_type": parentdoc.docType },
                        // { "doc_tracking.rev_current": parentdoc.revision },
                        { "doc_tracking.owner_archlead": parentdoc.ownerArchlead },
                        { "doc_tracking.design_contact": parentdoc.designContact },
                        { "doc_tracking.validation_contact": parentdoc.validationContact },
                        { "doc_tracking.tech_comm_support": parentdoc.techCommSupport },
                        { "send_mail": parentdoc.sendMail }
                    ];
                    console.log(request.var_args);
                    return request;
                };
                return HsdEsUpdateHelper;
            }());
            exports_1("HsdEsUpdateHelper", HsdEsUpdateHelper);
        }
    };
});
//# sourceMappingURL=hsdes-update.helper.js.map