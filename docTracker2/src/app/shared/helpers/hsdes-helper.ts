import { asEnumerable } from 'linq-es5';
import { HsdEsDocument } from "../../models/hsdes-document.model";
import { HsdEsParentDoc } from "../../models/hsdes-parentdoc.model";

export class HsdEsHelper {

      static Revisions: string[] = ['0.2', '0.5', '0.8', '1.0', '1.1'];
      static Statuses: any[] = [
            { id: "not_started", display: "Not Started", availableStatus: ["authoring_wip"] },
            { id: "waived", display: "Waived", availableStatus: [] },
            { id: "rejected", display: "Rejected", availableStatus: [] },
            { id: "authoring_wip", display: "Authoring WIP", availableStatus: ["reviewpdf_queue"]},
            { id: "reviewpdf_queue", display: "Review PDF Queue", availableStatus: ["review_candidate"] },
            { id: "review_candidate", display: "Review Candidate", availableStatus: ["review_complete"] },
            { id: "review_complete", display: "Review Complete", availableStatus: ["releasepdf_queue"] },
            { id: "releasepdf_queue", display: "Release PDF Queue", availableStatus: ["release_candidate"] },
            { id: "release_candidate", display: "Release Candidate", availableStatus: ["release"] },
            { id: "release", display: "Release", availableStatus: [] }
      ]
    
      static getCurrentRevision(docs: HsdEsDocument[]): string {

            var lastRevision = asEnumerable(docs).Where(d => d.status == 'waived' || d.status == 'release').OrderByDescending(d => d.revision).FirstOrDefault();
            if (!lastRevision) {
                  return '0.2';
            }
            return HsdEsHelper.getNextRevision(lastRevision.revision);
      }

      static getNextRevision(revision: string) {

            var i = this.Revisions.indexOf(revision);
            if (i == this.Revisions.length - 1) {
                  return revision;
            }
            return this.Revisions[i + 1];
      }

      public static availableStatusBasedOnCurrentStatus(currentStatus:string = null):string[]{
           if(!currentStatus)
                   return ["not_started"];
            
            return [currentStatus].concat(this.Statuses.find(status=> status.id === currentStatus).availableStatus);
      }

      public static createHierarchy(docs: HsdEsDocument[]): HsdEsParentDoc[] {
            var parents = [];
            asEnumerable(docs)
                  .Select(d => d.parentId).Distinct().ToArray()
                  .forEach(t => {
                        var parent = new HsdEsParentDoc();
                        parent.id = t;
                        parent.owner = docs.find(d => d.parentId == parent.id).owner;
                        parent.release = docs.find(d => d.parentId == parent.id).release;
                        parent.docType = docs.find(d => d.parentId == parent.id).docType;
                        parent.domain = docs.find(d => d.parentId == parent.id).domain;
                        parent.docName = docs.find(d => d.parentId == parent.id).docName;
                        parent.docs = asEnumerable(docs).Where((d: HsdEsDocument) =>
                              d.parentId == parent.id
                        ).OrderBy(d => d.revision).ToArray();
                        parent.currentRevision = this.getCurrentRevision(parent.docs);
                        parent.selectedRevision = parent.currentRevision;
                        parent.requestedRevision = parent.currentRevision;
                        parents.push(parent);
                  });

            return parents;
      }

      public static documentBaseQuery(): string {
            var sql = "SELECT"
                  + " id,"
                  + " title,"
                  + " release,"
                  + " release_affected,"
                  + " parent_id,"
                  + " domain,"
                  + " doc_tracking.rev_current AS 'rev_current',"
                  + " doc_tracking.doc_type AS 'doc_type',"
                  + " owner,"
                  + " doc_tracking.owner_archlead as 'owner_archlead',"
                  + " status,"
                  + " doc_tracking.release_url as 'release_url',"
                  + " doc_tracking.draft_url as 'draft_url',"
                  + " doc_tracking.review_url as 'review_url',"
                  + " doc_tracking.release_eta as 'release_eta',"
                  + " doc_tracking.review_date as 'review_date',"
                  + " doc_tracking.design_contact as 'design_contact',"
                  + " doc_tracking.validation_contact as 'validation_contact',"
                  + " doc_tracking.authoring_eta as 'authoring_eta',"
                  + " doc_tracking.authoring_done as 'authoring_done',"
                  + " doc_tracking.release_done as 'release_done',"
                  + " doc_tracking.tech_comm_support as 'tech_comm_support'"
                  + " WHERE"
                  + " doc_tracking.level = 'L2' AND"
                  + " doc_tracking.rev_current != '0.9' AND"
                  + " status != 'rejected' AND"
                  + " subject = 'doc_tracking'";
            return sql;
      }
      private static sortByClause(): string {
            return " SORTBY title,rev_current DESC";
      }

      public static buildDocumentQuery(releases: string[], queryReleaseAffected: boolean): string {
            if (releases.length < 1) {
                  return HsdEsHelper.documentBaseQuery().concat(this.sortByClause());
            }
            var query = HsdEsHelper.documentBaseQuery() + HsdEsHelper.buildReleaseStatement(releases);

            if (queryReleaseAffected) {
                  query = query + HsdEsHelper.buildQueryReleaseStatement(releases);
            }
            return query.concat(this.sortByClause());
      }

      public static buildReleaseStatement(releases: string[]): string {
            

            if (!releases || releases.length == 0)
                  return "";

            var release = " and release in ('";
            release += releases.join("','").concat("'");
            release = release + ")";
            return release;
      }


      public static buildQueryReleaseStatement(releases: string[]): string {

            var clause = "";
            releases.forEach(r => { clause += " OR release_affected contains '" + r + "'" });
            return clause;

      }

}