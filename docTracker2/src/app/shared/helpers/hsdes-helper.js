System.register(["linq-es2015", "../../models/hsdes-parentdoc.model"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var linq_es2015_1, hsdes_parentdoc_model_1, HsdEsHelper;
    return {
        setters: [
            function (linq_es2015_1_1) {
                linq_es2015_1 = linq_es2015_1_1;
            },
            function (hsdes_parentdoc_model_1_1) {
                hsdes_parentdoc_model_1 = hsdes_parentdoc_model_1_1;
            }
        ],
        execute: function () {
            HsdEsHelper = (function () {
                function HsdEsHelper() {
                }
                HsdEsHelper.getCurrentRevision = function (docs) {
                    var lastRevision = linq_es2015_1.asEnumerable(docs).Where(function (d) { return d.status == 'waived' || d.status == 'release'; }).OrderByDescending(function (d) { return d.revision; }).FirstOrDefault();
                    if (!lastRevision) {
                        return '0.2';
                    }
                    return HsdEsHelper.getNextRevision(lastRevision.revision);
                };
                HsdEsHelper.getNextRevision = function (revision) {
                    var i = this.Revisions.indexOf(revision);
                    if (i == this.Revisions.length - 1) {
                        return revision;
                    }
                    return this.Revisions[i + 1];
                };
                HsdEsHelper.availableStatusBasedOnCurrentStatus = function (currentStatus) {
                    if (currentStatus === void 0) { currentStatus = null; }
                    if (!currentStatus)
                        return ["not_started"];
                    return [currentStatus].concat(this.Statuses.find(function (status) { return status.id === currentStatus; }).availableStatus);
                };
                HsdEsHelper.createHierarchy = function (docs) {
                    var _this = this;
                    var parents = [];
                    linq_es2015_1.asEnumerable(docs)
                        .Select(function (d) { return d.parentId; }).Distinct().ToArray()
                        .forEach(function (t) {
                        var parent = new hsdes_parentdoc_model_1.HsdEsParentDoc();
                        parent.id = t;
                        parent.owner = docs.find(function (d) { return d.parentId == parent.id; }).owner;
                        parent.release = docs.find(function (d) { return d.parentId == parent.id; }).release;
                        parent.docType = docs.find(function (d) { return d.parentId == parent.id; }).docType;
                        parent.domain = docs.find(function (d) { return d.parentId == parent.id; }).domain;
                        parent.docName = docs.find(function (d) { return d.parentId == parent.id; }).docName;
                        parent.docs = linq_es2015_1.asEnumerable(docs).Where(function (d) {
                            return d.parentId == parent.id;
                        }).OrderBy(function (d) { return d.revision; }).ToArray();
                        parent.currentRevision = _this.getCurrentRevision(parent.docs);
                        parent.selectedRevision = parent.currentRevision;
                        parent.requestedRevision = parent.currentRevision;
                        parents.push(parent);
                    });
                    return parents;
                };
                HsdEsHelper.documentBaseQuery = function () {
                    var sql = "SELECT"
                        + " id,"
                        + " title,"
                        + " release,"
                        + " release_affected,"
                        + " parent_id,"
                        + " domain,"
                        + " doc_tracking.rev_current AS 'rev_current',"
                        + " doc_tracking.doc_type AS 'doc_type',"
                        + " owner,"
                        + " doc_tracking.owner_archlead as 'owner_archlead',"
                        + " status,"
                        + " doc_tracking.release_url as 'release_url',"
                        + " doc_tracking.draft_url as 'draft_url',"
                        + " doc_tracking.review_url as 'review_url',"
                        + " doc_tracking.release_eta as 'release_eta',"
                        + " doc_tracking.review_date as 'review_date',"
                        + " doc_tracking.design_contact as 'design_contact',"
                        + " doc_tracking.validation_contact as 'validation_contact',"
                        + " doc_tracking.authoring_eta as 'authoring_eta',"
                        + " doc_tracking.authoring_done as 'authoring_done',"
                        + " doc_tracking.release_done as 'release_done',"
                        + " doc_tracking.tech_comm_support as 'tech_comm_support'"
                        + " WHERE"
                        + " doc_tracking.level = 'L2' AND"
                        + " doc_tracking.rev_current != '0.9' AND"
                        + " status != 'rejected' AND"
                        + " subject = 'doc_tracking'";
                    return sql;
                };
                HsdEsHelper.sortByClause = function () {
                    return " SORTBY title,rev_current DESC";
                };
                HsdEsHelper.buildDocumentQuery = function (releases, queryReleaseAffected) {
                    if (releases.length < 1) {
                        return HsdEsHelper.documentBaseQuery().concat(this.sortByClause());
                    }
                    var query = HsdEsHelper.documentBaseQuery() + HsdEsHelper.buildReleaseStatement(releases);
                    if (queryReleaseAffected) {
                        query = query + HsdEsHelper.buildQueryReleaseStatement(releases);
                    }
                    return query.concat(this.sortByClause());
                };
                HsdEsHelper.buildReleaseStatement = function (releases) {
                    if (!releases || releases.length == 0)
                        return "";
                    var release = " and release in ('";
                    release += releases.join("','").concat("'");
                    release = release + ")";
                    return release;
                };
                HsdEsHelper.buildQueryReleaseStatement = function (releases) {
                    var clause = "";
                    releases.forEach(function (r) { clause += " OR release_affected contains '" + r + "'"; });
                    return clause;
                };
                return HsdEsHelper;
            }());
            HsdEsHelper.Revisions = ['0.2', '0.5', '0.8', '1.0', '1.1'];
            HsdEsHelper.Statuses = [
                { id: "not_started", display: "Not Started", availableStatus: ["authoring_wip"] },
                { id: "waived", display: "Waived", availableStatus: [] },
                { id: "rejected", display: "Rejected", availableStatus: [] },
                { id: "authoring_wip", display: "Authoring WIP", availableStatus: ["reviewpdf_queue"] },
                { id: "reviewpdf_queue", display: "Review PDF Queue", availableStatus: ["review_candidate"] },
                { id: "review_candidate", display: "Review Candidate", availableStatus: ["review_complete"] },
                { id: "review_complete", display: "Review Complete", availableStatus: ["releasepdf_queue"] },
                { id: "releasepdf_queue", display: "Release PDF Queue", availableStatus: ["release_candidate"] },
                { id: "release_candidate", display: "Release Candidate", availableStatus: ["release"] },
                { id: "release", display: "Release", availableStatus: [] }
            ];
            exports_1("HsdEsHelper", HsdEsHelper);
        }
    };
});
//# sourceMappingURL=hsdes-helper.js.map