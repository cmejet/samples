import { HsdEsRequest } from "../../models/hsdes-request.model";
import { HsdEsDocument } from "../../models/hsdes-document.model";


export class HsdEsInsertHelper {
    public static insertRecord(doc: HsdEsDocument): HsdEsRequest {

        var insertRequest = new HsdEsRequest();
        insertRequest.command = "insert_record_with_fetch";
        insertRequest.command_args.tenant = doc.tenant;
        insertRequest.command_args.subject = "doc_tracking";
        insertRequest.command_args.id = doc.id;
        insertRequest.var_args = [

            { "title": doc.docName || "" },
            { "doc_tracking.doc_type": doc.docType || "" },
            { "owner": doc.owner || "" },
            { "doc_tracking.owner_archlead": doc.ownerArchlead || "" },
            // { "release_affected": "" },
            { "domain": doc.domain || "" },
            { "status": doc.status || "" },
            { "doc_tracking.rev_current": doc.revision || "" },
            { "doc_tracking.design_contact": doc.designContact || ""},
            { "doc_tracking.validation_contact": doc.validationContact || "" },
            { "doc_tracking.validation_contact": doc.techCommSupport || ""},
            { "send_mail": doc.sendMail || false }
        ];

        if (doc.release)
            insertRequest.var_args.push({ "release": doc.release });
        if (doc.revision)
            insertRequest.var_args.push({ "doc_tracking.rev_current": doc.revision });
        return insertRequest;
    }

    public static insertParentRecord(doc: HsdEsDocument): HsdEsRequest {
        var request = HsdEsInsertHelper.insertRecord(doc);
        request.var_args.push({"parent_id":""});
        request.var_args.push({ "doc_tracking.level": "L1" });

        console.log(request.var_args);
        return request;

    }
    public static insertChildRecord(doc: HsdEsDocument): HsdEsRequest {
        var request = HsdEsInsertHelper.insertRecord(doc);
        request.var_args.push({ "parent_id": doc.parentId });
        request.var_args.push({ "doc_tracking.level": "L2" });
        return request;
    }
}