import { HsdEsRequest } from "../../models/hsdes-request.model";
import { HsdEsDocument } from "../../models/hsdes-document.model";
import { HsdEsParentDoc } from "../../models/hsdes-parentdoc.model"

export class HsdEsEditHelper {

    public static createUrlRequestObject(doc: HsdEsDocument): HsdEsRequest {
        var request = this.createBaseRequestObject(doc);
        request.var_args = [
            { "doc_tracking.release_url": this.convertUrl(doc.releaseUrl, "Release") },
            { "doc_tracking.review_url": this.convertUrl(doc.reviewUrl, "Review") },
            { "doc_tracking.draft_url": this.convertUrl(doc.draftUrl, "Draft") }
        ];
        return request;
    }

    private static convertUrl(url: string, type: string): string {
        var hsdEsUrlFormat = `<?xml version="1.0" ?><dict><a href="${url}" target="_blank">${type} URL</a></dict>`
        return hsdEsUrlFormat;
    }
    private static createBaseRequestObject(doc: HsdEsDocument): HsdEsRequest {
        var request = new HsdEsRequest();
        request.command = "update_record_with_fetch";
        request.command_args.tenant = doc.tenant;
        request.command_args.id = doc.id;
        request.var_args = [];
        return request;
    }
    public static createRejectRequestObject(doc: HsdEsDocument): HsdEsRequest {
        var request = this.createBaseRequestObject(doc);
        request.var_args.push({ "status": "rejected" });
        request.var_args.push({ "send_mail": false });
        return request;
    }
    public static createRejectParentRequestObject(parentdoc: HsdEsParentDoc): HsdEsRequest {
        var request = this.createBaseRequestObject(parentdoc.docs[0]);
        request.command_args.id = parentdoc.id;
        request.var_args.push({ "status": "rejected" });
        request.var_args.push({ "send_mail": false });
        return request;
    }
    public static createStatusRequestObject(doc: HsdEsDocument): HsdEsRequest {
        var request = this.createBaseRequestObject(doc);
        request.var_args.push({ "status": doc.status });
        if (this.needsDraftUrl(doc.status))
            request.var_args.push({ "doc_tracking.draft_url": this.convertUrl(doc.draftUrl, "Draft") });
        if (doc.status == "authoring_wip")
            request.var_args.push({ "doc_tracking.authoring_eta": doc.authoringEta });
        if (doc.status == "review_candidate")
            request.var_args.push({ "doc_tracking.review_url": this.convertUrl(doc.reviewUrl, "Review") });
        if (doc.status.indexOf("review_") > -1)
            request.var_args.push({ "doc_tracking.review_date": doc.reviewDate });
        if (this.needsReleaseEta(doc.status))
            request.var_args.push({ "doc_tracking.release_eta": doc.releaseEta });
        if (this.needsReleaseUrl(doc.status))
            request.var_args.push({ "doc_tracking.release_url": this.convertUrl(doc.releaseUrl, "Release") });
        return request;
    }
    private static needsDraftUrl(status: string): boolean {
        return status == "authoring_wip" || status == "reviewpdf_queue";
    }
    private static needsReleaseEta(status: string): boolean {
        return status == "review_complete" || status == "release_candidate";
    }
    private static needsReleaseUrl(status: string): boolean {
        return status == "release_candidate" || status == "release";
    }

    public static editParentRecord(parentdoc: HsdEsParentDoc, doc: HsdEsDocument): HsdEsRequest {
        var request = this.createBaseRequestObject(doc);
        request.var_args = [
            { "title": parentdoc.docs[0].docName },
            { "release": parentdoc.docs[0].release },
            { "release_affected": parentdoc.docs[0].releaseAffected },
            { "domain": parentdoc.docs[0].domain },
            { "owner": parentdoc.docs[0].owner },
            { "status": parentdoc.docs[0].status },
            { "doc_tracking.doc_type": parentdoc.docs[0].docType },
            { "doc_tracking.rev_current": parentdoc.docs[0].revision },
            { "doc_tracking.owner_archlead": parentdoc.docs[0].ownerArchlead },
            { "doc_tracking.design_contact": parentdoc.docs[0].designContact },
            { "doc_tracking.validation_contact": parentdoc.docs[0].validationContact },
            { "doc_tracking.tech_comm_support": parentdoc.docs[0].techCommSupport },
            { "send_mail": parentdoc.docs[0].sendMail }


        ];
        console.log(request.var_args);
        return request;

    }

}