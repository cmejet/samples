System.register(["../../models/hsdes-request.model"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var hsdes_request_model_1, HsdEsEditHelper;
    return {
        setters: [
            function (hsdes_request_model_1_1) {
                hsdes_request_model_1 = hsdes_request_model_1_1;
            }
        ],
        execute: function () {
            HsdEsEditHelper = (function () {
                function HsdEsEditHelper() {
                }
                HsdEsEditHelper.createUrlRequestObject = function (doc) {
                    var request = this.createBaseRequestObject(doc);
                    request.var_args = [
                        { "doc_tracking.release_url": this.convertUrl(doc.releaseUrl, "Release") },
                        { "doc_tracking.review_url": this.convertUrl(doc.reviewUrl, "Review") },
                        { "doc_tracking.draft_url": this.convertUrl(doc.draftUrl, "Draft") }
                    ];
                    return request;
                };
                HsdEsEditHelper.convertUrl = function (url, type) {
                    var hsdEsUrlFormat = "<?xml version=\"1.0\" ?><dict><a href=\"" + url + "\" target=\"_blank\">" + type + " URL</a></dict>";
                    return hsdEsUrlFormat;
                };
                HsdEsEditHelper.createBaseRequestObject = function (doc) {
                    var request = new hsdes_request_model_1.HsdEsRequest();
                    request.command = "update_record_with_fetch";
                    request.command_args.tenant = doc.tenant;
                    request.command_args.id = doc.id;
                    request.var_args = [];
                    return request;
                };
                HsdEsEditHelper.createRejectRequestObject = function (doc) {
                    var request = this.createBaseRequestObject(doc);
                    request.var_args.push({ "status": "rejected" });
                    request.var_args.push({ "send_mail": false });
                    return request;
                };
                HsdEsEditHelper.createRejectParentRequestObject = function (parentdoc) {
                    var request = this.createBaseRequestObject(parentdoc.docs[0]);
                    request.command_args.id = parentdoc.id;
                    request.var_args.push({ "status": "rejected" });
                    request.var_args.push({ "send_mail": false });
                    return request;
                };
                HsdEsEditHelper.createStatusRequestObject = function (doc) {
                    var request = this.createBaseRequestObject(doc);
                    request.var_args.push({ "status": doc.status });
                    if (this.needsDraftUrl(doc.status))
                        request.var_args.push({ "doc_tracking.draft_url": this.convertUrl(doc.draftUrl, "Draft") });
                    if (doc.status == "authoring_wip")
                        request.var_args.push({ "doc_tracking.authoring_eta": doc.authoringEta });
                    if (doc.status == "review_candidate")
                        request.var_args.push({ "doc_tracking.review_url": this.convertUrl(doc.reviewUrl, "Review") });
                    if (doc.status.indexOf("review_") > -1)
                        request.var_args.push({ "doc_tracking.review_date": doc.reviewDate });
                    if (this.needsReleaseEta(doc.status))
                        request.var_args.push({ "doc_tracking.release_eta": doc.releaseEta });
                    if (this.needsReleaseUrl(doc.status))
                        request.var_args.push({ "doc_tracking.release_url": this.convertUrl(doc.releaseUrl, "Release") });
                    return request;
                };
                HsdEsEditHelper.needsDraftUrl = function (status) {
                    return status == "authoring_wip" || status == "reviewpdf_queue";
                };
                HsdEsEditHelper.needsReleaseEta = function (status) {
                    return status == "review_complete" || status == "release_candidate";
                };
                HsdEsEditHelper.needsReleaseUrl = function (status) {
                    return status == "release_candidate" || status == "release";
                };
                HsdEsEditHelper.editParentRecord = function (parentdoc, doc) {
                    var request = this.createBaseRequestObject(doc);
                    request.var_args = [
                        { "title": parentdoc.docs[0].docName },
                        { "release": parentdoc.docs[0].release },
                        { "release_affected": parentdoc.docs[0].releaseAffected },
                        { "domain": parentdoc.docs[0].domain },
                        { "owner": parentdoc.docs[0].owner },
                        { "status": parentdoc.docs[0].status },
                        { "doc_tracking.doc_type": parentdoc.docs[0].docType },
                        { "doc_tracking.rev_current": parentdoc.docs[0].revision },
                        { "doc_tracking.owner_archlead": parentdoc.docs[0].ownerArchlead },
                        { "doc_tracking.design_contact": parentdoc.docs[0].designContact },
                        { "doc_tracking.validation_contact": parentdoc.docs[0].validationContact },
                        { "doc_tracking.tech_comm_support": parentdoc.docs[0].techCommSupport },
                        { "send_mail": parentdoc.docs[0].sendMail }
                    ];
                    console.log(request.var_args);
                    return request;
                };
                return HsdEsEditHelper;
            }());
            exports_1("HsdEsEditHelper", HsdEsEditHelper);
        }
    };
});
//# sourceMappingURL=hsdes-edit.helper.js.map