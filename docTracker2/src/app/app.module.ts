import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';

import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { Ng2TableModule } from 'ng2-table';
import { Ng2BootstrapModule } from 'ngx-bootstrap';
import { ToastModule } from 'ng2-toastr/ng2-toastr';

import { RevisionPipe } from './shared/pipes/revision.pipe';

import { AppComponent } from './app.component';
import { HeaderComponent } from './modules/shared/header.component';
import { DocTrackerComponent } from './modules/doc-tracker/doc-tracker.component';
import { QueryComponent } from './modules/doc-tracker/query/query.component';
import { SpecIndexComponent } from './modules/doc-tracker/spec-index/spec-index.component';
import { ImportSpecComponent } from './modules/doc-tracker/import/import-spec.component';
import { HsdEsService } from './services/hsd-es.service';
import { WorkerService } from './services/worker.service';
import { NewHasComponent } from './modules/doc-tracker/new-has/new-has.component';
import { UpdateLinksComponent } from './modules/doc-tracker/query/modals/update-links.component';
import { UpdateStatusComponent } from './modules/doc-tracker/query/modals/update-status.component';
import { EditActionComponent } from './modules/doc-tracker/query/modals/edit-action.component';
import { RejectActionComponent } from './modules/doc-tracker/query/modals/reject-action.component';
import { WaiveRevisionsComponent } from './modules/doc-tracker/query/modals/waive-revisions.component';
import { WorkerSearchComponent } from './modules/shared/worker-search.component';
@NgModule({
  declarations: [
    RevisionPipe,
    AppComponent,
    HeaderComponent,
    DocTrackerComponent,
    QueryComponent,
    SpecIndexComponent,
    ImportSpecComponent,
    NewHasComponent,
    UpdateLinksComponent,
    UpdateStatusComponent,
    EditActionComponent,
    RejectActionComponent,
    WaiveRevisionsComponent,
    WorkerSearchComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    MultiselectDropdownModule,
    Ng2TableModule,
    Ng2BootstrapModule.forRoot(),
    ToastModule.forRoot(),
    AppRoutingModule
  ],
  providers: [
    HsdEsService,
    WorkerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
