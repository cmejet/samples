System.register(["@angular/core", "@angular/http", "rxjs/add/operator/toPromise", "../models/hsdes-document.model", "../models/hsdes-product.model", "../models/user.model", "../shared/helpers/hsdes-helper", "../shared/helpers/hsdes-update.helper", "../shared/helpers/hsdes-insert.helper", "../models/hsdes-domain.model", "../models/hsdes-doctype.model", "../models/hsdes-releaseAffected.model"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, http_1, hsdes_document_model_1, hsdes_product_model_1, user_model_1, hsdes_helper_1, hsdes_update_helper_1, hsdes_insert_helper_1, hsdes_domain_model_1, hsdes_doctype_model_1, hsdes_releaseAffected_model_1, HsdEsService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {
            },
            function (hsdes_document_model_1_1) {
                hsdes_document_model_1 = hsdes_document_model_1_1;
            },
            function (hsdes_product_model_1_1) {
                hsdes_product_model_1 = hsdes_product_model_1_1;
            },
            function (user_model_1_1) {
                user_model_1 = user_model_1_1;
            },
            function (hsdes_helper_1_1) {
                hsdes_helper_1 = hsdes_helper_1_1;
            },
            function (hsdes_update_helper_1_1) {
                hsdes_update_helper_1 = hsdes_update_helper_1_1;
            },
            function (hsdes_insert_helper_1_1) {
                hsdes_insert_helper_1 = hsdes_insert_helper_1_1;
            },
            function (hsdes_domain_model_1_1) {
                hsdes_domain_model_1 = hsdes_domain_model_1_1;
            },
            function (hsdes_doctype_model_1_1) {
                hsdes_doctype_model_1 = hsdes_doctype_model_1_1;
            },
            function (hsdes_releaseAffected_model_1_1) {
                hsdes_releaseAffected_model_1 = hsdes_releaseAffected_model_1_1;
            }
        ],
        execute: function () {
            HsdEsService = (function () {
                function HsdEsService(http) {
                    this.http = http;
                    this.hsdEsQueryUrl = "https://hsdes-pre.intel.com/rest/query/execution/eql?verbose=false&start_at=1";
                    // private hsdEsQueryUrl: string = "https://hsdes.intel.com/rest/query/execution/eql?verbose=false&start_at=1"
                    this.hsdEsWsBaseUrl = "https://hsdes-pre.intel.com/ws/ESService";
                    this.headers = new http_1.Headers({
                        'Content-Type': 'application/json'
                    });
                }
                HsdEsService.prototype.getReleaseAffectedList = function () {
                    return this.http.post('https://hsdes.intel.com/rest/lookup/release_affected?subject=doc_tracking&tenantlist=phablet&start_at=1', {}, { headers: this.headers, withCredentials: true })
                        .toPromise()
                        .then(function (response) {
                        return hsdes_releaseAffected_model_1.HsdEsReleaseAffected.parseArray(response.json().data);
                    })
                        .catch(this.handleError);
                };
                HsdEsService.prototype.getDomainList = function () {
                    return this.http.post('https://hsdes.intel.com/rest/lookup/domain?subject=doc_tracking&tenantlist=phablet&start_at=1', {}, { headers: this.headers, withCredentials: true })
                        .toPromise()
                        .then(function (response) {
                        return hsdes_domain_model_1.HsdEsDomain.parseArray(response.json().data);
                    })
                        .catch(this.handleError);
                };
                HsdEsService.prototype.getDocTypeList = function () {
                    return this.http.post('https://hsdes.intel.com/rest/lookup/doc_tracking.doc_type?subject=doc_tracking&tenantlist=phablet&start_at=1', {}, { headers: this.headers, withCredentials: true })
                        .toPromise()
                        .then(function (response) {
                        return hsdes_doctype_model_1.HsdEsDocType.parseArray(response.json().data);
                    })
                        .catch(this.handleError);
                };
                HsdEsService.prototype.getReleases = function () {
                    return this.http.post(this.hsdEsQueryUrl, JSON.stringify({ "eql": "Select id,esapp_data.data where subject = 'esapp_data' and esapp_data.appID = 'ARCHBENCH.SPEC_DASHBOARD'" }), { headers: this.headers, withCredentials: true })
                        .toPromise()
                        .then(function (response) {
                        return hsdes_product_model_1.HsdEsProduct.parseArray(JSON.parse(response.json().data[0]['esapp_data.data']).products);
                    })
                        .catch(this.handleError);
                };
                HsdEsService.prototype.getDocs = function (releases, queryReleaseAffected) {
                    if (queryReleaseAffected === void 0) { queryReleaseAffected = false; }
                    return this.http.post(this.hsdEsQueryUrl, JSON.stringify({ "eql": hsdes_helper_1.HsdEsHelper.buildDocumentQuery(releases, queryReleaseAffected) }), { headers: this.headers, withCredentials: true })
                        .toPromise()
                        .then(function (response) {
                        return hsdes_document_model_1.HsdEsDocument.parseArray(response.json().data);
                    })
                        .catch(this.handleError);
                };
                HsdEsService.prototype.updateDocUrls = function (doc) {
                    return this.http.post(this.hsdEsWsBaseUrl, JSON.stringify({ "requests": [hsdes_update_helper_1.HsdEsUpdateHelper.createUrlRequestObject(doc)] }), { headers: this.headers, withCredentials: true })
                        .toPromise()
                        .then(function (response) {
                        return response.json().data;
                    })
                        .catch(this.handleError);
                };
                HsdEsService.prototype.updateDocStatus = function (doc) {
                    return this.http.post(this.hsdEsWsBaseUrl, JSON.stringify({ "requests": [hsdes_update_helper_1.HsdEsUpdateHelper.createStatusRequestObject(doc)] }), { headers: this.headers, withCredentials: true })
                        .toPromise()
                        .then(function (response) {
                        return response.json().data;
                    })
                        .catch(this.handleError);
                };
                HsdEsService.prototype.rejectDocumentParent = function (parentdoc) {
                    return this.http.post(this.hsdEsWsBaseUrl, JSON.stringify({ "requests": [hsdes_update_helper_1.HsdEsUpdateHelper.createRejectParentRequestObject(parentdoc)] }), { headers: this.headers, withCredentials: true })
                        .toPromise()
                        .then(function (response) {
                        return response.json().data;
                    })
                        .catch(this.handleError);
                };
                HsdEsService.prototype.rejectDocumentChild = function (doc) {
                    return this.http.post(this.hsdEsWsBaseUrl, JSON.stringify({ "requests": [hsdes_update_helper_1.HsdEsUpdateHelper.createRejectRequestObject(doc)] }), { headers: this.headers, withCredentials: true })
                        .toPromise()
                        .then(function (response) {
                        return response.json().data;
                    })
                        .catch(this.handleError);
                };
                HsdEsService.prototype.waiveDocument = function (doc) {
                    return this.http.post(this.hsdEsWsBaseUrl, JSON.stringify({ "requests": [hsdes_update_helper_1.HsdEsUpdateHelper.createWaiveRequestObject(doc)] }), { headers: this.headers, withCredentials: true })
                        .toPromise()
                        .then(function (response) {
                        return response.json().data;
                    })
                        .catch(this.handleError);
                };
                HsdEsService.prototype.insertParentHas = function (doc) {
                    return this.http.post(this.hsdEsWsBaseUrl, JSON.stringify({ "requests": [hsdes_insert_helper_1.HsdEsInsertHelper.insertParentRecord(doc)] }), { headers: this.headers, withCredentials: true })
                        .toPromise()
                        .then(function (response) {
                        return response.json();
                    })
                        .catch(this.handleError);
                };
                HsdEsService.prototype.insertChildHas = function (doc) {
                    return this.http.post(this.hsdEsWsBaseUrl, JSON.stringify({ "requests": [hsdes_insert_helper_1.HsdEsInsertHelper.insertChildRecord(doc)] }), { headers: this.headers, withCredentials: true })
                        .toPromise()
                        .then(function (response) {
                        return response.json();
                    })
                        .catch(this.handleError);
                };
                HsdEsService.prototype.editActionParent = function (parentdoc) {
                    return this.http.post(this.hsdEsWsBaseUrl, JSON.stringify({ "requests": [hsdes_update_helper_1.HsdEsUpdateHelper.editParentRecord(parentdoc)] }), { headers: this.headers, withCredentials: true })
                        .toPromise()
                        .then(function (response) {
                        console.log(response.json());
                        return response.json();
                    })
                        .catch(this.handleError);
                };
                HsdEsService.prototype.getUser = function () {
                    return this.http.get("https://hsdes.intel.com/rest/user", { headers: this.headers, withCredentials: true })
                        .toPromise()
                        .then(function (response) {
                        return user_model_1.User.parse(response.json());
                    })
                        .catch(this.handleError);
                };
                HsdEsService.prototype.handleError = function (error) {
                    console.error('An error occurred', error); // for demo purposes only
                    return Promise.reject(error.message || error);
                };
                return HsdEsService;
            }());
            HsdEsService = __decorate([
                core_1.Injectable(),
                __metadata("design:paramtypes", [http_1.Http])
            ], HsdEsService);
            exports_1("HsdEsService", HsdEsService);
        }
    };
});
//# sourceMappingURL=hsd-es.service.js.map