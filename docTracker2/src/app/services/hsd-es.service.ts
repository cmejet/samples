import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { HsdEsDocument } from "../models/hsdes-document.model";
import { HsdEsProduct } from "../models/hsdes-product.model";
import { User } from "../models/user.model";
import { HsdEsHelper } from "../shared/helpers/hsdes-helper";
import { HsdEsUpdateHelper } from "../shared/helpers/hsdes-update.helper";
import { HsdEsEditHelper } from "../shared/helpers/hsdes-edit.helper";
import { HsdEsInsertHelper } from "../shared/helpers/hsdes-insert.helper";
import { HsdEsParentDoc } from "../models/hsdes-parentdoc.model";
import { HsdEsDomain } from "../models/hsdes-domain.model";
import { HsdEsDocType } from "../models/hsdes-doctype.model";
import { HsdEsReleaseAffected } from "../models/hsdes-releaseAffected.model";


@Injectable()

export class HsdEsService {
  private hsdEsQueryUrl: string = "https://hsdes-pre.intel.com/rest/query/execution/eql?verbose=false&start_at=1"
  // private hsdEsQueryUrl: string = "https://hsdes.intel.com/rest/query/execution/eql?verbose=false&start_at=1"
  private hsdEsWsBaseUrl: string = "https://hsdes-pre.intel.com/ws/ESService";

  constructor(private http: Http) { }

  getReleaseAffectedList(): Promise<HsdEsReleaseAffected[]> {
    return this.http.post('https://hsdes.intel.com/rest/lookup/release_affected?subject=doc_tracking&tenantlist=phablet&start_at=1', {},
      { headers: this.headers, withCredentials: true })
      .toPromise()
      .then(response =>
        HsdEsReleaseAffected.parseArray(response.json().data)
      )
      .catch(this.handleError);
  }

  getDomainList(): Promise<HsdEsDomain[]> {
    return this.http.post('https://hsdes.intel.com/rest/lookup/domain?subject=doc_tracking&tenantlist=phablet&start_at=1', {},
      { headers: this.headers, withCredentials: true })
      .toPromise()
      .then(response =>
        HsdEsDomain.parseArray(response.json().data)
      )
      .catch(this.handleError);
  }

  getDocTypeList(): Promise<HsdEsDocType[]> {
    return this.http.post('https://hsdes.intel.com/rest/lookup/doc_tracking.doc_type?subject=doc_tracking&tenantlist=phablet&start_at=1', {},
      { headers: this.headers, withCredentials: true })
      .toPromise()
      .then(response =>
        HsdEsDocType.parseArray(response.json().data)
      )
      .catch(this.handleError);
  }

  getReleases(): Promise<HsdEsProduct[]> {
    return this.http.post(this.hsdEsQueryUrl,
      JSON.stringify({ "eql": "Select id,esapp_data.data where subject = 'esapp_data' and esapp_data.appID = 'ARCHBENCH.SPEC_DASHBOARD'" }),
      { headers: this.headers, withCredentials: true }
    )
      .toPromise()
      .then(response =>
        HsdEsProduct.parseArray(JSON.parse(response.json().data[0]['esapp_data.data']).products)
      )
      .catch(this.handleError);
  }

  getDocs(releases: string[], queryReleaseAffected: boolean = false): Promise<HsdEsDocument[]> {
    return this.http.post(this.hsdEsQueryUrl,
      JSON.stringify({ "eql": HsdEsHelper.buildDocumentQuery(releases, queryReleaseAffected) }),
      { headers: this.headers, withCredentials: true }
    )
      .toPromise()
      .then(response =>
        HsdEsDocument.parseArray(response.json().data)
      )
      .catch(this.handleError);
  }

  updateDocUrls(doc: HsdEsDocument): Promise<any> {
    return this.http.post(this.hsdEsWsBaseUrl,
      JSON.stringify({ "requests": [HsdEsUpdateHelper.createUrlRequestObject(doc)] }),
      { headers: this.headers, withCredentials: true }
    )
      .toPromise()
      .then(response =>
        response.json().data as any
      )
      .catch(this.handleError);
  }

  updateDocStatus(doc: HsdEsDocument): Promise<any> {
    return this.http.post(this.hsdEsWsBaseUrl,
      JSON.stringify({ "requests": [HsdEsUpdateHelper.createStatusRequestObject(doc)] }),
      { headers: this.headers, withCredentials: true }
    )
      .toPromise()
      .then(response =>
        response.json().data as any
      )
      .catch(this.handleError);
  }
  rejectDocumentParent(parentdoc: HsdEsParentDoc): Promise<any> {
    return this.http.post(this.hsdEsWsBaseUrl,
      JSON.stringify({ "requests": [HsdEsUpdateHelper.createRejectParentRequestObject(parentdoc)] }),
      { headers: this.headers, withCredentials: true }
    )
      .toPromise()
      .then(response =>
        response.json().data as any
      )
      .catch(this.handleError);
  }
  rejectDocumentChild(doc: HsdEsDocument): Promise<any> {
    return this.http.post(this.hsdEsWsBaseUrl,
      JSON.stringify({ "requests": [HsdEsUpdateHelper.createRejectRequestObject(doc)] }),
      { headers: this.headers, withCredentials: true }
    )
      .toPromise()
      .then(response =>
        response.json().data as any
      )
      .catch(this.handleError);
  }
  waiveDocument(doc: HsdEsDocument): Promise<any> {
    return this.http.post(this.hsdEsWsBaseUrl,
      JSON.stringify({ "requests": [HsdEsUpdateHelper.createWaiveRequestObject(doc)] }),
      { headers: this.headers, withCredentials: true }
    )
      .toPromise()
      .then(response =>
        response.json().data as any
      )
      .catch(this.handleError);
  }
  insertParentHas(doc: HsdEsDocument): Promise<any> {
    return this.http.post(this.hsdEsWsBaseUrl,
      JSON.stringify({ "requests": [HsdEsInsertHelper.insertParentRecord(doc)] }),
      { headers: this.headers, withCredentials: true }
    )
      .toPromise()
      .then(response =>
        response.json() as any
      )
      .catch(this.handleError);
  }

  insertChildHas(doc: HsdEsDocument): Promise<any> {
    return this.http.post(this.hsdEsWsBaseUrl,
      JSON.stringify({ "requests": [HsdEsInsertHelper.insertChildRecord(doc)] }),
      { headers: this.headers, withCredentials: true }
    )
      .toPromise()
      .then(response =>
        response.json() as any
      )
      .catch(this.handleError);
  }
  editActionParent(parentdoc: HsdEsParentDoc): Promise<any> {
    return this.http.post(this.hsdEsWsBaseUrl,
      JSON.stringify({ "requests": [HsdEsUpdateHelper.editParentRecord(parentdoc)] }),
      { headers: this.headers, withCredentials: true }
    )
      .toPromise()
      .then(response => {
        console.log(response.json());
        return response.json() as any
      })
      .catch(this.handleError);
  }


  getUser(): Promise<User> {
    return this.http.get("https://hsdes.intel.com/rest/user",
      { headers: this.headers, withCredentials: true }
    )
      .toPromise()
      .then(response =>
        User.parse(response.json())
      )
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  private headers = new Headers({
    'Content-Type': 'application/json'
  });
}