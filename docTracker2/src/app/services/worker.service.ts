import { Injectable } from '@angular/core';
import { Headers, Jsonp } from '@angular/http';


import 'rxjs/add/operator/toPromise';

import{User} from '../models/user.model';

@Injectable()

export class WorkerService {
    constructor(private jsonp:Jsonp) { }

    public searchWorker(value: string):Promise<User[]> {
        return this.jsonp.get('http://thegrid.intel.com/api/omni/search/?q=employee:%20' + value + '&pageSize=50&callback=JSONP_CALLBACK'
        )
            .toPromise()
            .then(response =>{
              return User.parseArray(response.json())
            })
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        // this.toastr.error('An error occurred' + error.message || error)
        return Promise.reject(error.message || error);
    }
}