System.register(["@angular/core", "@angular/http", "rxjs/add/operator/toPromise", "../models/user.model"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, http_1, user_model_1, WorkerService;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {
            },
            function (user_model_1_1) {
                user_model_1 = user_model_1_1;
            }
        ],
        execute: function () {
            WorkerService = (function () {
                function WorkerService(jsonp) {
                    this.jsonp = jsonp;
                }
                WorkerService.prototype.searchWorker = function (value) {
                    return this.jsonp.get('http://thegrid.intel.com/api/omni/search/?q=employee:%20' + value + '&pageSize=50&callback=JSONP_CALLBACK')
                        .toPromise()
                        .then(function (response) {
                        return user_model_1.User.parseArray(response.json());
                    })
                        .catch(this.handleError);
                };
                WorkerService.prototype.handleError = function (error) {
                    console.error('An error occurred', error); // for demo purposes only
                    // this.toastr.error('An error occurred' + error.message || error)
                    return Promise.reject(error.message || error);
                };
                return WorkerService;
            }());
            WorkerService = __decorate([
                core_1.Injectable(),
                __metadata("design:paramtypes", [http_1.Jsonp])
            ], WorkerService);
            exports_1("WorkerService", WorkerService);
        }
    };
});
//# sourceMappingURL=worker.service.js.map