System.register(["linq-es2015"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var linq_es2015_1, User;
    return {
        setters: [
            function (linq_es2015_1_1) {
                linq_es2015_1 = linq_es2015_1_1;
            }
        ],
        execute: function () {
            User = (function () {
                function User(idsid, firstName, lastName, nickName, wwid, emailAddress) {
                    if (idsid === void 0) { idsid = null; }
                    if (firstName === void 0) { firstName = null; }
                    if (lastName === void 0) { lastName = null; }
                    if (nickName === void 0) { nickName = null; }
                    if (wwid === void 0) { wwid = null; }
                    if (emailAddress === void 0) { emailAddress = null; }
                    this.idsid = idsid;
                    this.firstName = firstName;
                    this.lastName = lastName;
                    this.nickName = nickName;
                    this.wwid = wwid;
                    this.emailAddress = emailAddress;
                }
                Object.defineProperty(User.prototype, "fullName", {
                    get: function () {
                        return this.lastName.concat(', ').concat(this.nickName.trim().length > 0 ? this.nickName : this.firstName);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(User.prototype, "photoLink", {
                    get: function () {
                        return 'https://photos.intel.com/images/' + this.wwid + '.jpg';
                    },
                    enumerable: true,
                    configurable: true
                });
                User.parse = function (json) {
                    return new User(json.user);
                };
                User.parseArray = function (json) {
                    return linq_es2015_1.asEnumerable(json.hits.data).Select(function (u) {
                        return new User(u.document.idsid, u.document.firstName, u.document.lastName, u.document.nickName, u.document.id, u.document.email);
                    }).ToArray();
                };
                return User;
            }());
            exports_1("User", User);
        }
    };
});
//# sourceMappingURL=user.model.js.map