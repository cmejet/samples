import { asEnumerable } from 'linq-es5';
import {HsdEsHelper} from '../shared/helpers/hsdes-helper';
import {HsdEsDocument} from './hsdes-document.model';

export class HsdEsParentDoc {
    public id: string;
    public docName: string; 
    public owner: string;
    public release: string;
    public docType: string;
    public domain: string;
    public selectedAction: string;
    public currentRevision: string;
    public selectedRevision: string;
    public requestedRevision: string;
    public ownerArchlead: string;
    public designContact : string;
    public validationContact  : string;
    public techCommSupport : string;
    public sendMail : string;
    public releaseAffected : string;

    public docs: HsdEsDocument[];
    public isExpanded: boolean;

    constructor() {
        this.isExpanded = false;
        this.docs = [];
        HsdEsHelper.Revisions.forEach(r=> {
            var doc = new HsdEsDocument();
            doc.revision = r;
            doc.status = "not_started";
            this.docs.push(doc);
        });
    }

    public get selectedDoc():HsdEsDocument{
        return this.docs.find(d=> d.revision == this.selectedRevision);
    }
    
    public get latestReleased():HsdEsDocument{
       var doc= asEnumerable(this.docs).OrderByDescending(d=>d.revision).FirstOrDefault(d=>d.status=="release");
       return doc? doc: new HsdEsDocument();
    }
    public get revsToWaive():HsdEsDocument[]{
        return asEnumerable(this.docs).Where(d=>d.revision < this.requestedRevision && !d.isClosed).ToArray();
    }
}
