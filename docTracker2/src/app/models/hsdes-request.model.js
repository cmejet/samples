System.register([], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var HsdEsRequest, HsdCommandArguments;
    return {
        setters: [],
        execute: function () {
            HsdEsRequest = (function () {
                function HsdEsRequest() {
                    this.command_args = new HsdCommandArguments();
                    this.tran_id = ""; // "scopeRouter:app:hsd_return:3";
                    this.api_client = "ARCHBENCH";
                }
                return HsdEsRequest;
            }());
            exports_1("HsdEsRequest", HsdEsRequest);
            HsdCommandArguments = (function () {
                function HsdCommandArguments() {
                    this.id = null;
                    this.tenant = null;
                    this.subject = "doc_tracking";
                }
                return HsdCommandArguments;
            }());
            exports_1("HsdCommandArguments", HsdCommandArguments);
        }
    };
});
//# sourceMappingURL=hsdes-request.model.js.map