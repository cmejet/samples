import { asEnumerable } from 'linq-es5';
export class HsdEsDocType {
    public id: string;
    public name: string;
    constructor() {

    }
    public static parse(json): HsdEsDocType {
        var item = new HsdEsDocType();
        item.id = json["sys_lookup.value"];
        item.name = item.id;
        return item;
    }

    public static parseArray(json: any): HsdEsDocType[] {
        var items = [];
        json.forEach(item => {
            items.push(this.parse(item));
        });
        return asEnumerable(items).OrderBy(i => i.name).ToArray()
    }
}