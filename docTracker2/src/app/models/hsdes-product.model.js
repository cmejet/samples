System.register(["linq-es2015"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var linq_es2015_1, HsdEsProduct;
    return {
        setters: [
            function (linq_es2015_1_1) {
                linq_es2015_1 = linq_es2015_1_1;
            }
        ],
        execute: function () {
            HsdEsProduct = (function () {
                function HsdEsProduct() {
                }
                Object.defineProperty(HsdEsProduct.prototype, "displayName", {
                    get: function () {
                        return this.pmName.trim().length > 0 ? this.pmName : 'UNKNOWN';
                    },
                    enumerable: true,
                    configurable: true
                });
                HsdEsProduct.parse = function (json) {
                    var prod = new HsdEsProduct();
                    prod.name = json.name;
                    prod.tenant = json.tenant;
                    prod.pmName = json.pm_name;
                    prod.pmEmail = json.pm_email;
                    prod.isCregDisabled = json.creg_disabled;
                    prod.id = json.name;
                    return prod;
                };
                HsdEsProduct.parseArray = function (json) {
                    var _this = this;
                    var items = [];
                    json.forEach(function (item) { return items.push(_this.parse(item)); });
                    return linq_es2015_1.asEnumerable(items).OrderBy(function (d) { return d.name; }).ToArray();
                };
                return HsdEsProduct;
            }());
            exports_1("HsdEsProduct", HsdEsProduct);
        }
    };
});
//# sourceMappingURL=hsdes-product.model.js.map