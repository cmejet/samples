import { asEnumerable } from 'linq-es5';
export class HsdEsProduct {
    public name: string;
    public tenant: string;
    public pmName: string;
    public pmEmail: string;
    public isCregDisabled: boolean;
    public id: string;

    public get displayName(): string {
        return this.pmName.trim().length > 0 ? this.pmName : 'UNKNOWN';
    }

    public static parse(json: any): HsdEsProduct {
        var prod = new HsdEsProduct();
        prod.name = json.name;
        prod.tenant = json.tenant;
        prod.pmName = json.pm_name;
        prod.pmEmail = json.pm_email;
        prod.isCregDisabled = json.creg_disabled;
        prod.id = json.name;
        return prod;
    }
    public static parseArray(json: any): HsdEsProduct[] {
        var items = [];

        json.forEach(item => items.push(this.parse(item)));
        return asEnumerable(items).OrderBy(d => d.name).ToArray();
    }

}