import { HsdEsHelper } from '../shared/helpers/hsdes-helper';
import {asEnumerable} from 'linq-es5';
export class HsdEsDocument {

    public id: string;
    public parentId: string;
    public action: string;
    public release: string;
    public docName: string;
    public domain: string;
    public docType: string;
    public owner: string;
    public revision: string;
    public status: string;
    public currentStatus: string;
    public authoringEta: Date;
    public reviewDate: Date;
    public releaseEta: Date;
    public url: string;
    public draftUrl: string;
    public releaseUrl: string;
    public reviewUrl: string;
    public isCurrent: boolean;
    public tenant: string;
    public subject: string;
    public ownerArchlead: string;
    public designContact: string;
    public validationContact: string;
    public techCommSupport: string;
    public sendMail: string;
    public currentRevision: string;
    public releaseAffected: string;
    constructor() {
        this.revision = "";
        this.status = "";
    }
    public get isReleased(): boolean {
        return this.status == "release";
    }
    public get isClosed(): boolean {
        var closedStatuses = asEnumerable(HsdEsHelper.Statuses).Where(s => s.availableStatus == 0).Select(s=> s.id).ToArray();
        return closedStatuses.indexOf(this.status) >= 0;
    }
    public get newIsValid(): boolean {
        if (!this.docType || !this.owner || !this.ownerArchlead || !this.docName) // mandatory fields in New HAS form
            return false;
        return true;
    }
    public static parseUrl(tag: string): string {
        if (!tag)
            return "";
        if (tag.indexOf("href") < 0)
            return "";

        var url = "";
        if (tag.indexOf("href") >= 0) {
            var startIndex = tag.indexOf("href=\"") + 6;
            var endIndex = tag.indexOf("\" target");
            url = tag.substring(startIndex, endIndex);
        } else {
            url = tag;
        }
        return url;
    }

    private static getUrlForStatus(item): string {
        var url = "";
        if (['release', 'closed', 'release_candidate'].indexOf(item.status) > -1) {
            url = this.parseUrl(item.release_url);
        } else if (['review_candidate', 'review_complete', 'releasepdf_queue'].indexOf(item.status) > -1) {
            url = this.parseUrl(item.review_url);
        } else if (['reviewpdf_queue', 'authoring_wip'].indexOf(item.status) > -1) {
            url = this.parseUrl(item.draft_url);
        }
        return url;
    }
    public get hsdEsUrl(): string {
        return "https://hsdes.intel.com/appstore/article/#/" + this.id;
    }
    public get availableStatuses(): string[] {
        return HsdEsHelper.availableStatusBasedOnCurrentStatus(this.currentStatus);
    }
    public get nextEta(): string {
        switch (this.status) {
            case "waived":
            case "rejected":
                return "";
            case "not_started":
            case "authoring_wip":
                return this.authoringEta ? this.authoringEta.toString() : "";
            default:
                return this.releaseEta ? this.releaseEta.toString() : "";
        }
    }
    public get nextEtaExpired(): boolean {
        var now = new Date();
        var eta = new Date(this.nextEta);
        return eta < now;
    }
    public get nextEtaType(): string {
        switch (this.status) {
            case "not_started":
            case "authoring_wip":
                return "authoring";
            default:
                return "release";
        }
    }
    public static parse(json: any): HsdEsDocument {
        var doc = new HsdEsDocument;
        doc.id = json.id;
        doc.parentId = json.parent_id;
        doc.action = json.action;
        doc.release = json.release;
        doc.docName = json.title;
        doc.domain = json.domain;
        doc.docType = json.doc_type;
        doc.owner = json.owner;
        doc.revision = json.rev_current;
        doc.status = json.status;
        doc.currentStatus = json.status;
        doc.authoringEta = json.authoring_eta;
        doc.reviewDate = json.review_date;
        doc.releaseEta = json.release_eta;
        doc.url = this.getUrlForStatus(json);
        doc.draftUrl = this.parseUrl(json.draft_url);
        doc.releaseUrl = this.parseUrl(json.release_url);
        doc.reviewUrl = this.parseUrl(json.review_url);
        doc.isCurrent = json.is_current;
        doc.tenant = json.tenant;
        doc.subject = json.subject;
        return doc;
    }
    public static parseArray(json: any[]): HsdEsDocument[] {
        var docs = [];
        json.forEach(item => docs.push(this.parse(item)));
        return docs;
    }
    public get displayDraftUrl(): boolean {
        return this.status == "authoring_wip" || this.status == "reviewpdf_queue";
    }
    public get displayAuthoringEta(): boolean {
        return this.status == "authoring_wip";
    }
    public get displayReviewUrl(): boolean {
        return this.status == "review_candidate";
    }
    public get displayReviewDate(): boolean {
        return this.status == "review_candidate" || this.status == "review_complete";
    }
    public get displayReleaseEta(): boolean {
        return this.status == "review_complete" || this.status == "release_candidate";
    }
    public get displayReleaseUrl(): boolean {
        return this.status == "release_candidate" || this.status == "release";
    }

    public get requireDraftUrl(): boolean {
        return this.status == "reviewpdf_queue";
    }
    public get requireAuthoringEta(): boolean {
        return this.status == "authoring_wip";
    }
    public get requireReviewUrl(): boolean {
        return this.status == "review_candidate";
    }
    public get requireReviewDate(): boolean {
        return this.status == "review_candidate" || this.status == "review_complete";
    }
    public get requireReleaseEta(): boolean {
        return this.status == "release_candidate";
    }
    public get requireReleaseUrl(): boolean {
        return this.status == "release_candidate" || this.status == "release";
    }

    public get validateAuthoringWip(): boolean {
        return this.draftUrl != undefined && this.authoringEta != undefined && Date.parse(this.authoringEta.toString()) && this.draftUrl.length > 0;
    }
    public get validateReviewCandidate(): boolean {
        return this.reviewUrl != undefined && this.reviewDate != undefined && this.reviewUrl.length > 0;
    }
    public get validateReleaseCandidate(): boolean {
        return this.releaseEta != undefined && this.releaseUrl != undefined && this.releaseUrl.length > 0;
    }
    public get validateRelease(): boolean {
        return this.releaseUrl != undefined && this.releaseUrl.length > 0;
    }

    public get isStatusUpdateValid(): boolean {
        switch (this.status) {
            case 'authoring_wip':
                return this.validateAuthoringWip;
            case 'review_candidate':
                return this.validateReviewCandidate;
            case 'release_candidate':
                return this.validateReleaseCandidate;
            case 'release' || 'review_complete':
                return this.validateRelease;
            default:
                return true;
        }
    }
}