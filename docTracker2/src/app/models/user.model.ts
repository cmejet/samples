import { asEnumerable } from 'linq-es5';

export class User {
    constructor(
        public idsid: string = null,
        public firstName: string = null,
        public lastName: string = null,
        public nickName: string = null,
        public wwid: string = null,
        public emailAddress: string = null
    ) { }
    public get fullName(): string {
        return this.lastName.concat(', ').concat(this.nickName.trim().length > 0 ? this.nickName : this.firstName);
    }
    public get photoLink(): string {
        
        return 'https://photos.intel.com/images/' + this.wwid + '.jpg';
    }
    public static parse(json: any): User {
        return new User(json.user);
    }

    public static parseArray(json: any): User[] {
        return asEnumerable(json.hits.data).Select((u: any) =>
            new User(u.document.idsid, u.document.firstName, u.document.lastName, u.document.nickName, u.document.id, u.document.email)
        ).ToArray();
    }
}