export class HsdEsRequest {
    public tran_id: string;
    public api_client:string;
    public command: string;
    public command_args: HsdCommandArguments;
    public var_args: any[];

    constructor() {
        this.command_args = new HsdCommandArguments();
        this.tran_id ="";// "scopeRouter:app:hsd_return:3";
        this.api_client="ARCHBENCH";
    }
}
export class HsdCommandArguments {
    public id: string;
    public tenant: string;
    public subject: string;

    constructor() {
        this.id = null;
        this.tenant = null;
        this.subject = "doc_tracking";
    }
}