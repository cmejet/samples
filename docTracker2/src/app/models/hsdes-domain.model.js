System.register(["linq-es2015"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var linq_es2015_1, HsdEsDomain;
    return {
        setters: [
            function (linq_es2015_1_1) {
                linq_es2015_1 = linq_es2015_1_1;
            }
        ],
        execute: function () {
            HsdEsDomain = (function () {
                function HsdEsDomain() {
                }
                HsdEsDomain.parse = function (json) {
                    var item = new HsdEsDomain();
                    item.id = json["sys_lookup.value"];
                    item.name = item.id;
                    return item;
                };
                HsdEsDomain.parseArray = function (json) {
                    var _this = this;
                    var items = [];
                    json.forEach(function (item) {
                        items.push(_this.parse(item));
                    });
                    return linq_es2015_1.asEnumerable(items).OrderBy(function (i) { return i.name; }).ToArray();
                };
                return HsdEsDomain;
            }());
            exports_1("HsdEsDomain", HsdEsDomain);
        }
    };
});
//# sourceMappingURL=hsdes-domain.model.js.map