System.register(["../shared/helpers/hsdes-helper", "linq-es2015"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var hsdes_helper_1, linq_es2015_1, HsdEsDocument;
    return {
        setters: [
            function (hsdes_helper_1_1) {
                hsdes_helper_1 = hsdes_helper_1_1;
            },
            function (linq_es2015_1_1) {
                linq_es2015_1 = linq_es2015_1_1;
            }
        ],
        execute: function () {
            HsdEsDocument = (function () {
                function HsdEsDocument() {
                    this.revision = "";
                    this.status = "";
                }
                Object.defineProperty(HsdEsDocument.prototype, "isReleased", {
                    get: function () {
                        return this.status == "release";
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "isClosed", {
                    get: function () {
                        var closedStatuses = linq_es2015_1.asEnumerable(hsdes_helper_1.HsdEsHelper.Statuses).Where(function (s) { return s.availableStatus == 0; }).Select(function (s) { return s.id; }).ToArray();
                        return closedStatuses.indexOf(this.status) >= 0;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "newIsValid", {
                    get: function () {
                        if (!this.docType || !this.owner || !this.ownerArchlead || !this.docName)
                            return false;
                        return true;
                    },
                    enumerable: true,
                    configurable: true
                });
                HsdEsDocument.parseUrl = function (tag) {
                    if (!tag)
                        return "";
                    if (tag.indexOf("href") < 0)
                        return "";
                    var url = "";
                    if (tag.indexOf("href") >= 0) {
                        var startIndex = tag.indexOf("href=\"") + 6;
                        var endIndex = tag.indexOf("\" target");
                        url = tag.substring(startIndex, endIndex);
                    }
                    else {
                        url = tag;
                    }
                    return url;
                };
                HsdEsDocument.getUrlForStatus = function (item) {
                    var url = "";
                    if (['release', 'closed', 'release_candidate'].indexOf(item.status) > -1) {
                        url = this.parseUrl(item.release_url);
                    }
                    else if (['review_candidate', 'review_complete', 'releasepdf_queue'].indexOf(item.status) > -1) {
                        url = this.parseUrl(item.review_url);
                    }
                    else if (['reviewpdf_queue', 'authoring_wip'].indexOf(item.status) > -1) {
                        url = this.parseUrl(item.draft_url);
                    }
                    return url;
                };
                Object.defineProperty(HsdEsDocument.prototype, "hsdEsUrl", {
                    get: function () {
                        return "https://hsdes.intel.com/appstore/article/#/" + this.id;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "availableStatuses", {
                    get: function () {
                        return hsdes_helper_1.HsdEsHelper.availableStatusBasedOnCurrentStatus(this.currentStatus);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "nextEta", {
                    get: function () {
                        switch (this.status) {
                            case "waived":
                            case "rejected":
                                return "";
                            case "not_started":
                            case "authoring_wip":
                                return this.authoringEta ? this.authoringEta.toString() : "";
                            default:
                                return this.releaseEta ? this.releaseEta.toString() : "";
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "nextEtaExpired", {
                    get: function () {
                        var now = new Date();
                        var eta = new Date(this.nextEta);
                        return eta < now;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "nextEtaType", {
                    get: function () {
                        switch (this.status) {
                            case "not_started":
                            case "authoring_wip":
                                return "authoring";
                            default:
                                return "release";
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                HsdEsDocument.parse = function (json) {
                    var doc = new HsdEsDocument;
                    doc.id = json.id;
                    doc.parentId = json.parent_id;
                    doc.action = json.action;
                    doc.release = json.release;
                    doc.docName = json.title;
                    doc.domain = json.domain;
                    doc.docType = json.doc_type;
                    doc.owner = json.owner;
                    doc.revision = json.rev_current;
                    doc.status = json.status;
                    doc.currentStatus = json.status;
                    doc.authoringEta = json.authoring_eta;
                    doc.reviewDate = json.review_date;
                    doc.releaseEta = json.release_eta;
                    doc.url = this.getUrlForStatus(json);
                    doc.draftUrl = this.parseUrl(json.draft_url);
                    doc.releaseUrl = this.parseUrl(json.release_url);
                    doc.reviewUrl = this.parseUrl(json.review_url);
                    doc.isCurrent = json.is_current;
                    doc.tenant = json.tenant;
                    doc.subject = json.subject;
                    return doc;
                };
                HsdEsDocument.parseArray = function (json) {
                    var _this = this;
                    var docs = [];
                    json.forEach(function (item) { return docs.push(_this.parse(item)); });
                    return docs;
                };
                Object.defineProperty(HsdEsDocument.prototype, "displayDraftUrl", {
                    get: function () {
                        return this.status == "authoring_wip" || this.status == "reviewpdf_queue";
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "displayAuthoringEta", {
                    get: function () {
                        return this.status == "authoring_wip";
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "displayReviewUrl", {
                    get: function () {
                        return this.status == "review_candidate";
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "displayReviewDate", {
                    get: function () {
                        return this.status == "review_candidate" || this.status == "review_complete";
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "displayReleaseEta", {
                    get: function () {
                        return this.status == "review_complete" || this.status == "release_candidate";
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "displayReleaseUrl", {
                    get: function () {
                        return this.status == "release_candidate" || this.status == "release";
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "requireDraftUrl", {
                    get: function () {
                        return this.status == "reviewpdf_queue";
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "requireAuthoringEta", {
                    get: function () {
                        return this.status == "authoring_wip";
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "requireReviewUrl", {
                    get: function () {
                        return this.status == "review_candidate";
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "requireReviewDate", {
                    get: function () {
                        return this.status == "review_candidate" || this.status == "review_complete";
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "requireReleaseEta", {
                    get: function () {
                        return this.status == "release_candidate";
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "requireReleaseUrl", {
                    get: function () {
                        return this.status == "release_candidate" || this.status == "release";
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "validateAuthoringWip", {
                    get: function () {
                        return this.draftUrl != undefined && this.authoringEta != undefined && Date.parse(this.authoringEta.toString()) && this.draftUrl.length > 0;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "validateReviewCandidate", {
                    get: function () {
                        return this.reviewUrl != undefined && this.reviewDate != undefined && this.reviewUrl.length > 0;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "validateReleaseCandidate", {
                    get: function () {
                        return this.releaseEta != undefined && this.releaseUrl != undefined && this.releaseUrl.length > 0;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "validateRelease", {
                    get: function () {
                        return this.releaseUrl != undefined && this.releaseUrl.length > 0;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsDocument.prototype, "isStatusUpdateValid", {
                    get: function () {
                        switch (this.status) {
                            case 'authoring_wip':
                                return this.validateAuthoringWip;
                            case 'review_candidate':
                                return this.validateReviewCandidate;
                            case 'release_candidate':
                                return this.validateReleaseCandidate;
                            case 'release' || 'review_complete':
                                return this.validateRelease;
                            default:
                                return true;
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                return HsdEsDocument;
            }());
            exports_1("HsdEsDocument", HsdEsDocument);
        }
    };
});
//# sourceMappingURL=hsdes-document.model.js.map