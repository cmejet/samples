import { asEnumerable } from 'linq-es5';
export class HsdEsDomain {
     public id: string;
    public name: string;
    constructor() {

    }
    public static parse(json): HsdEsDomain {
        var item = new HsdEsDomain();
        item.id = json["sys_lookup.value"];
        item.name = item.id;
        return item;
    }

    public static parseArray(json: any): HsdEsDomain[] {
        var items = [];
        json.forEach(item => {
            items.push(this.parse(item));
        });
        return asEnumerable(items).OrderBy(i => i.name).ToArray()
    }
}