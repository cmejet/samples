System.register(["linq-es2015"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var linq_es2015_1, HsdEsReleaseAffected;
    return {
        setters: [
            function (linq_es2015_1_1) {
                linq_es2015_1 = linq_es2015_1_1;
            }
        ],
        execute: function () {
            HsdEsReleaseAffected = (function () {
                function HsdEsReleaseAffected() {
                }
                HsdEsReleaseAffected.parse = function (json) {
                    var item = new HsdEsReleaseAffected();
                    item.id = json["sys_lookup.value"];
                    item.name = item.id;
                    return item;
                };
                HsdEsReleaseAffected.parseArray = function (json) {
                    var _this = this;
                    var items = [];
                    json.forEach(function (item) {
                        items.push(_this.parse(item));
                    });
                    return linq_es2015_1.asEnumerable(items).OrderBy(function (i) { return i.name; }).ToArray();
                };
                return HsdEsReleaseAffected;
            }());
            exports_1("HsdEsReleaseAffected", HsdEsReleaseAffected);
        }
    };
});
//# sourceMappingURL=hsdes-releaseAffected.model.js.map