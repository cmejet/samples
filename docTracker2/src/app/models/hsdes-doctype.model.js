System.register(["linq-es2015"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var linq_es2015_1, HsdEsDocType;
    return {
        setters: [
            function (linq_es2015_1_1) {
                linq_es2015_1 = linq_es2015_1_1;
            }
        ],
        execute: function () {
            HsdEsDocType = (function () {
                function HsdEsDocType() {
                }
                HsdEsDocType.parse = function (json) {
                    var item = new HsdEsDocType();
                    item.id = json["sys_lookup.value"];
                    item.name = item.id;
                    return item;
                };
                HsdEsDocType.parseArray = function (json) {
                    var _this = this;
                    var items = [];
                    json.forEach(function (item) {
                        items.push(_this.parse(item));
                    });
                    return linq_es2015_1.asEnumerable(items).OrderBy(function (i) { return i.name; }).ToArray();
                };
                return HsdEsDocType;
            }());
            exports_1("HsdEsDocType", HsdEsDocType);
        }
    };
});
//# sourceMappingURL=hsdes-doctype.model.js.map