System.register(["linq-es2015", "../shared/helpers/hsdes-helper", "./hsdes-document.model"], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var linq_es2015_1, hsdes_helper_1, hsdes_document_model_1, HsdEsParentDoc;
    return {
        setters: [
            function (linq_es2015_1_1) {
                linq_es2015_1 = linq_es2015_1_1;
            },
            function (hsdes_helper_1_1) {
                hsdes_helper_1 = hsdes_helper_1_1;
            },
            function (hsdes_document_model_1_1) {
                hsdes_document_model_1 = hsdes_document_model_1_1;
            }
        ],
        execute: function () {
            HsdEsParentDoc = (function () {
                function HsdEsParentDoc() {
                    var _this = this;
                    this.isExpanded = false;
                    this.docs = [];
                    hsdes_helper_1.HsdEsHelper.Revisions.forEach(function (r) {
                        var doc = new hsdes_document_model_1.HsdEsDocument();
                        doc.revision = r;
                        doc.status = "not_started";
                        _this.docs.push(doc);
                    });
                }
                Object.defineProperty(HsdEsParentDoc.prototype, "selectedDoc", {
                    get: function () {
                        var _this = this;
                        return this.docs.find(function (d) { return d.revision == _this.selectedRevision; });
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsParentDoc.prototype, "latestReleased", {
                    get: function () {
                        var doc = linq_es2015_1.asEnumerable(this.docs).OrderByDescending(function (d) { return d.revision; }).FirstOrDefault(function (d) { return d.status == "release"; });
                        return doc ? doc : new hsdes_document_model_1.HsdEsDocument();
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(HsdEsParentDoc.prototype, "revsToWaive", {
                    get: function () {
                        var _this = this;
                        return linq_es2015_1.asEnumerable(this.docs).Where(function (d) { return d.revision < _this.requestedRevision && !d.isClosed; }).ToArray();
                    },
                    enumerable: true,
                    configurable: true
                });
                return HsdEsParentDoc;
            }());
            exports_1("HsdEsParentDoc", HsdEsParentDoc);
        }
    };
});
//# sourceMappingURL=hsdes-parentdoc.model.js.map