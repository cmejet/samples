import { asEnumerable } from 'linq-es5';
export class HsdEsReleaseAffected {
    public id: string;
    public name: string;
    constructor() {

    }
    public static parse(json): HsdEsReleaseAffected {
        var item = new HsdEsReleaseAffected();
        item.id = json["sys_lookup.value"];
        item.name = item.id;
        return item;
    }

    public static parseArray(json: any): HsdEsReleaseAffected[] {
        var items = [];
        json.forEach(item => {
            items.push(this.parse(item));
        });
        return asEnumerable(items).OrderBy(i => i.name).ToArray()
    }
}