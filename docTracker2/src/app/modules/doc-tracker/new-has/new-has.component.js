System.register(["@angular/core", "../../../services/hsd-es.service", "../../../models/hsdes-document.model", "../../../models/hsdes-product.model", "../../../shared/helpers/hsdes-helper", "ng2-toastr/ng2-toastr"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, hsd_es_service_1, hsdes_document_model_1, hsdes_product_model_1, hsdes_helper_1, ng2_toastr_1, NewHasComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (hsd_es_service_1_1) {
                hsd_es_service_1 = hsd_es_service_1_1;
            },
            function (hsdes_document_model_1_1) {
                hsdes_document_model_1 = hsdes_document_model_1_1;
            },
            function (hsdes_product_model_1_1) {
                hsdes_product_model_1 = hsdes_product_model_1_1;
            },
            function (hsdes_helper_1_1) {
                hsdes_helper_1 = hsdes_helper_1_1;
            },
            function (ng2_toastr_1_1) {
                ng2_toastr_1 = ng2_toastr_1_1;
            }
        ],
        execute: function () {
            NewHasComponent = (function () {
                function NewHasComponent(hsdES, toastr, vcr) {
                    this.hsdES = hsdES;
                    this.toastr = toastr;
                    this.onClosed = new core_1.EventEmitter();
                    this.toastr.setRootViewContainerRef(vcr);
                }
                NewHasComponent.prototype.cancel = function () {
                    this.toastr.info("Cancel New Has");
                    this.onClosed.emit();
                };
                NewHasComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.doc = new hsdes_document_model_1.HsdEsDocument(); // initialised
                    this.doc.release = this.selectedRelease.name;
                    this.doc.tenant = this.selectedRelease.tenant;
                    this.docTypeSettings = {
                        enableSearch: true,
                        showCheckAll: false,
                        showUncheckAll: false,
                        selectionLimit: 1,
                        autoUnselect: true,
                        checkedStyle: 'fontawesome'
                    };
                    this.domainSettings = {
                        enableSearch: true,
                        showCheckAll: false,
                        showUncheckAll: false,
                        selectionLimit: 1,
                        autoUnselect: true,
                        checkedStyle: 'fontawesome'
                    };
                    this.releaseAffectedSettings = {
                        enableSearch: true,
                        showCheckAll: false,
                        showUncheckAll: false,
                        autoUnselect: true,
                        checkedStyle: 'fontawesome'
                    };
                    this.hsdES.getDocTypeList().then(function (response) {
                        _this.docTypes = response;
                    });
                    this.hsdES.getDomainList().then(function (response) {
                        _this.domains = response;
                    });
                    this.hsdES.getReleaseAffectedList().then(function (response) {
                        _this.releaseAffected = response;
                    });
                };
                NewHasComponent.prototype.onSubmit = function () {
                    var _this = this;
                    this.doc.docType = this.doc.docType[0];
                    this.hsdES.insertParentHas(this.doc).then(function (response) {
                        _this.doc.parentId = response.responses[0].result_params.newId;
                        _this.doc.status = "not_started";
                        hsdes_helper_1.HsdEsHelper.Revisions.forEach(function (r) {
                            _this.doc.revision = r;
                            _this.hsdES.insertChildHas(_this.doc);
                        });
                    });
                    this.toastr.success("Form Submitted");
                    this.onClosed.emit();
                };
                NewHasComponent.prototype.showAddForm = function () {
                    var showForm = true;
                    return showForm;
                };
                NewHasComponent.prototype.hideAddForm = function () {
                    var showForm = false;
                    return showForm;
                };
                return NewHasComponent;
            }());
            __decorate([
                core_1.Input(),
                __metadata("design:type", hsdes_product_model_1.HsdEsProduct)
            ], NewHasComponent.prototype, "selectedRelease", void 0);
            __decorate([
                core_1.Output(),
                __metadata("design:type", Object)
            ], NewHasComponent.prototype, "onClosed", void 0);
            NewHasComponent = __decorate([
                core_1.Component({
                    selector: 'new-has',
                    templateUrl: './new-has.component.html',
                }),
                __metadata("design:paramtypes", [hsd_es_service_1.HsdEsService, ng2_toastr_1.ToastsManager, core_1.ViewContainerRef])
            ], NewHasComponent);
            exports_1("NewHasComponent", NewHasComponent);
        }
    };
});
//# sourceMappingURL=new-has.component.js.map