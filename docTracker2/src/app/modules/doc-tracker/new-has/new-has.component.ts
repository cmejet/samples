import { Component, ViewContainerRef, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { OnInit } from '@angular/core';
import { IMultiSelectSettings } from 'angular-2-dropdown-multiselect';
import { HsdEsService } from '../../../services/hsd-es.service';
import { HsdEsDocument } from "../../../models/hsdes-document.model";
import { HsdEsParentDoc } from "../../../models/hsdes-parentdoc.model";
import { HsdEsProduct } from "../../../models/hsdes-product.model";
import { HsdEsDocType } from '../../../models/hsdes-doctype.model';
import { HsdEsDomain } from '../../../models/hsdes-domain.model';
import { HsdEsReleaseAffected } from '../../../models/hsdes-releaseAffected.model';
import { HsdEsHelper } from "../../../shared/helpers/hsdes-helper";
import { HsdEsInsertHelper } from '../../../shared/helpers/hsdes-insert.helper';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
    selector: 'new-has',
    templateUrl: './new-has.component.html',
})

export class NewHasComponent implements OnInit {
    @Input() selectedRelease: HsdEsProduct;
    @Output() onClosed = new EventEmitter<void>();

    public doc: HsdEsDocument; //defined
    public domains: HsdEsDomain[];
    public docTypes: HsdEsDocType[];
    public releaseAffected: HsdEsReleaseAffected[];
    public docTypeSettings: IMultiSelectSettings;
    public domainSettings: IMultiSelectSettings;
    public releaseAffectedSettings: IMultiSelectSettings;

    public selectedDocType: any;
    public selectedDomain: any;
    public selectedReleaseAffected: any;

    constructor(private hsdES: HsdEsService, public toastr: ToastsManager, vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    public cancel(): void {
        this.toastr.info("Cancel New Has");
        this.onClosed.emit();
    }
    ngOnInit() {
        this.doc = new HsdEsDocument(); // initialised
        this.doc.release = this.selectedRelease.name;
        this.doc.tenant = this.selectedRelease.tenant;
        this.docTypeSettings = {
            enableSearch: true,
            showCheckAll: false,
            showUncheckAll: false,
            selectionLimit: 1,
            autoUnselect: true,
            checkedStyle: 'fontawesome'
        };
        this.domainSettings = {
            enableSearch: true,
            showCheckAll: false,
            showUncheckAll: false,
            selectionLimit: 1,
            autoUnselect: true,
            checkedStyle: 'fontawesome'
        };
        this.releaseAffectedSettings = {
            enableSearch: true,
            showCheckAll: false,
            showUncheckAll: false,
            
            autoUnselect: true,
            checkedStyle: 'fontawesome'
        };

        this.hsdES.getDocTypeList().then(response => {
            this.docTypes = response;
        });
        this.hsdES.getDomainList().then(response => {
            this.domains = response;
        });
        this.hsdES.getReleaseAffectedList().then(response => {
            this.releaseAffected = response;
        });
    }

    public onSubmit(): void {
        this.doc.docType = this.doc.docType[0];
        this.hsdES.insertParentHas(this.doc).then((response) => {
            this.doc.parentId = response.responses[0].result_params.newId;
            this.doc.status="not_started";
            
            HsdEsHelper.Revisions.forEach(r => {
                this.doc.revision = r;
                this.hsdES.insertChildHas(this.doc);
            });
        });

        this.toastr.success("Form Submitted");
        this.onClosed.emit();
    }


    public showAddForm(): boolean {
        var showForm = true;
        return showForm;
    }
    public hideAddForm(): boolean {
        var showForm = false;
        return showForm;
    }
}
