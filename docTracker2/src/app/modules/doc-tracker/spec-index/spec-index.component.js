System.register(["@angular/core", "@angular/router", "../../../services/hsd-es.service", "../../../shared/helpers/hsdes-helper"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, router_1, hsd_es_service_1, hsdes_helper_1, SpecIndexComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (hsd_es_service_1_1) {
                hsd_es_service_1 = hsd_es_service_1_1;
            },
            function (hsdes_helper_1_1) {
                hsdes_helper_1 = hsdes_helper_1_1;
            }
        ],
        execute: function () {
            SpecIndexComponent = (function () {
                function SpecIndexComponent(hsdES, router, route) {
                    this.hsdES = hsdES;
                    this.router = router;
                    this.route = route;
                    //ngTable Config Settings
                    this.page = 1;
                    this.itemsPerPage = 10;
                    this.pageSizes = [10, 25, 50, 100];
                    this.maxSize = 5;
                    this.numPages = 1;
                    this.length = 0;
                    this.filters = {
                        docName: { filterString: '', placeholder: '' },
                        domain: { filterString: '', placeholder: '' },
                        docType: { filterString: '', placeholder: '' },
                        owner: { filterString: '', placeholder: '' },
                        latestRevision: { filterString: '', placeholder: 'Latest Rev' },
                        lastReleased: { filterString: '', placeholder: 'Last Released' },
                        nextEta: { filterString: '', placeholder: '' },
                    };
                    this.config = {
                        paging: true,
                        className: ['table-striped', 'table-bordered']
                    };
                }
                SpecIndexComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.parents = [];
                    this.viewCurrent = false; // this.route.snapshot.queryParams['current']? this.route.snapshot.queryParams['current']:false;
                    this.hsdES.getReleases().then(function (response) {
                        _this.releases = response;
                        if (_this.route.snapshot.queryParams['release'])
                            _this.selectedReleases = [_this.route.snapshot.queryParams['release']];
                        if (_this.selectedReleases && _this.selectedReleases.length == 1)
                            _this.onSubmit();
                    });
                    this.mySettings = {
                        enableSearch: true,
                        selectionLimit: 1,
                        autoUnselect: true,
                        checkedStyle: 'fontawesome',
                        closeOnSelect: true
                    };
                    this.myTexts = {
                        defaultTitle: "Select Release"
                    };
                };
                SpecIndexComponent.prototype.onSubmit = function () {
                    var _this = this;
                    this.router.navigate(['/specindex']); //resets active route
                    this.router.navigate(['/specindex'], { queryParams: { release: this.selectedReleases[0], current: this.viewCurrent } });
                    this.selectedRelease = this.releases.find(function (r) { return r.name == _this.selectedReleases[0]; });
                    this.hsdES.getDocs(this.selectedReleases).then(function (response) {
                        _this.parents = hsdes_helper_1.HsdEsHelper.createHierarchy(response);
                        _this.onChangeTable();
                    });
                };
                SpecIndexComponent.prototype.onChangeTable = function (page) {
                    if (page === void 0) { page = { page: this.page, itemsPerPage: this.itemsPerPage }; }
                    var data = this.parents;
                    var filteredData = this.changeFilter(data);
                    var sortedData = this.changeSort(filteredData);
                    this.rows = page && this.config.paging ? this.changePage(page, sortedData) : sortedData;
                    this.length = sortedData.length;
                };
                SpecIndexComponent.prototype.changePage = function (page, data) {
                    if (data === void 0) { data = this.parents; }
                    var start = (page.page - 1) * page.itemsPerPage;
                    var end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
                    return data.slice(start, end);
                };
                SpecIndexComponent.prototype.changeSort = function (data) {
                    if (!this.config.sorting) {
                        return data;
                    }
                    var columns = this.config.sorting.columns || [];
                    var columnName = void 0;
                    var sort = void 0;
                    for (var i = 0; i < columns.length; i++) {
                        if (columns[i].sort !== '') {
                            columnName = columns[i].name;
                            sort = columns[i].sort;
                        }
                    }
                    if (!columnName) {
                        return data;
                    }
                    // simple sorting
                    return data.sort(function (previous, current) {
                        if (previous[columnName] > current[columnName]) {
                            return sort === 'desc' ? -1 : 1;
                        }
                        else if (previous[columnName] < current[columnName]) {
                            return sort === 'asc' ? -1 : 1;
                        }
                        return 0;
                    });
                };
                SpecIndexComponent.prototype.changeFilter = function (data) {
                    var _this = this;
                    var filteredData = data.filter(function (d) {
                        return d.docName.toUpperCase().indexOf(_this.filters.docName.filterString.toUpperCase()) >= 0
                            && d.domain.toUpperCase().indexOf(_this.filters.domain.filterString.toUpperCase()) >= 0
                            && d.docType.toUpperCase().indexOf(_this.filters.docType.filterString.toUpperCase()) >= 0
                            && d.owner.toUpperCase().indexOf(_this.filters.owner.filterString.toUpperCase()) >= 0
                            && d.latestReleased.revision.toUpperCase().indexOf(_this.filters.lastReleased.filterString.toUpperCase()) >= 0
                            && d.selectedDoc.revision.toUpperCase().indexOf(_this.filters.latestRevision.filterString.toUpperCase()) >= 0
                            && d.selectedDoc.nextEta.toUpperCase().indexOf(_this.filters.nextEta.filterString.toUpperCase()) >= 0;
                    });
                    return filteredData;
                };
                return SpecIndexComponent;
            }());
            SpecIndexComponent = __decorate([
                core_1.Component({
                    selector: 'spec-index',
                    templateUrl: './spec-index.component.html',
                    styleUrls: ['./doc-tracker.spec-index.css']
                }),
                __metadata("design:paramtypes", [hsd_es_service_1.HsdEsService, router_1.Router, router_1.ActivatedRoute])
            ], SpecIndexComponent);
            exports_1("SpecIndexComponent", SpecIndexComponent);
        }
    };
});
//# sourceMappingURL=spec-index.component.js.map