import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { IMultiSelectSettings,IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import { HsdEsService } from '../../../services/hsd-es.service';
import { HsdEsParentDoc } from "../../../models/hsdes-parentdoc.model";
import { HsdEsProduct } from "../../../models/hsdes-product.model";
import { HsdEsHelper } from "../../../shared/helpers/hsdes-helper";

@Component({
    selector: 'spec-index',
    templateUrl: './spec-index.component.html',
    styleUrls: ['./doc-tracker.spec-index.css']
})

export class SpecIndexComponent implements OnInit {
    public releases: HsdEsProduct[];
    public selectedRelease: HsdEsProduct;
    public selectedReleases: string[];
    public viewCurrent: boolean;
    public parents: HsdEsParentDoc[];
    public mySettings: IMultiSelectSettings;
    public myTexts: IMultiSelectTexts;
    //ngTable Config Settings
    public page: number = 1;
    public itemsPerPage: number = 10;
    public pageSizes:number[]=[10,25,50,100];
    public maxSize: number = 5;
    public numPages: number = 1;
    public length: number = 0;
    public filters: any = {
        docName: { filterString: '', placeholder: '' },
        domain: { filterString: '', placeholder: '' },
        docType: { filterString: '', placeholder: '' },
        owner: { filterString: '', placeholder: '' },
        latestRevision: { filterString: '', placeholder: 'Latest Rev' },
        lastReleased: { filterString: '', placeholder: 'Last Released' },
        nextEta: { filterString: '', placeholder: '' },
    };
    public rows: any[];

    public config: any = {
        paging: true,
        className: ['table-striped', 'table-bordered']
    }

    constructor(private hsdES: HsdEsService, private router: Router, private route: ActivatedRoute) {

    }
    ngOnInit() {
        this.parents = [];
        this.viewCurrent = false;// this.route.snapshot.queryParams['current']? this.route.snapshot.queryParams['current']:false;

        this.hsdES.getReleases().then(response => {
            this.releases = response;
            if (this.route.snapshot.queryParams['release'])
                this.selectedReleases = [this.route.snapshot.queryParams['release']];
            if (this.selectedReleases && this.selectedReleases.length == 1)
                this.onSubmit();
        })

        this.mySettings = {
            enableSearch: true,
            selectionLimit: 1,
            autoUnselect: true,
            checkedStyle: 'fontawesome',
            closeOnSelect:true
        };
        this.myTexts={
            defaultTitle:"Select Release"            
        }
    }
    public onSubmit(): void {
        this.router.navigate(['/specindex']);//resets active route
        this.router.navigate(['/specindex'], { queryParams: { release: this.selectedReleases[0], current: this.viewCurrent } });
        this.selectedRelease = this.releases.find(r => r.name == this.selectedReleases[0]);
        this.hsdES.getDocs(this.selectedReleases).then(response => {
            this.parents = HsdEsHelper.createHierarchy(response);
            this.onChangeTable();
        });
    }
    public onChangeTable(page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
        var data = this.parents;
        let filteredData = this.changeFilter(data);
        let sortedData = this.changeSort(filteredData);
        this.rows = page && this.config.paging ? this.changePage(page, sortedData) : sortedData;
        this.length = sortedData.length;
    }
    public changePage(page: any, data: Array<any> = this.parents): Array<any> {
        let start = (page.page - 1) * page.itemsPerPage;
        let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
        return data.slice(start, end);
    }

    public changeSort(data: any): any {
        if (!this.config.sorting) {
            return data;
        }

        let columns = this.config.sorting.columns || [];
        let columnName: string = void 0;
        let sort: string = void 0;

        for (let i = 0; i < columns.length; i++) {
            if (columns[i].sort !== '') {
                columnName = columns[i].name;
                sort = columns[i].sort;
            }
        }

        if (!columnName) {
            return data;
        }

        // simple sorting
        return data.sort((previous: any, current: any) => {
            if (previous[columnName] > current[columnName]) {
                return sort === 'desc' ? -1 : 1;
            } else if (previous[columnName] < current[columnName]) {
                return sort === 'asc' ? -1 : 1;
            }
            return 0;
        });
    }

    public changeFilter(data: any): any {
        var filteredData = data.filter(d =>
            d.docName.toUpperCase().indexOf(this.filters.docName.filterString.toUpperCase()) >= 0
            && d.domain.toUpperCase().indexOf(this.filters.domain.filterString.toUpperCase()) >= 0
            && d.docType.toUpperCase().indexOf(this.filters.docType.filterString.toUpperCase()) >= 0
            && d.owner.toUpperCase().indexOf(this.filters.owner.filterString.toUpperCase()) >= 0
            && d.latestReleased.revision.toUpperCase().indexOf(this.filters.lastReleased.filterString.toUpperCase()) >= 0
            && d.selectedDoc.revision.toUpperCase().indexOf(this.filters.latestRevision.filterString.toUpperCase()) >= 0
            && d.selectedDoc.nextEta.toUpperCase().indexOf(this.filters.nextEta.filterString.toUpperCase()) >= 0
        )
        return filteredData;
    }
}