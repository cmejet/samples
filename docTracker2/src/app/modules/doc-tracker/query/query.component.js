System.register(["@angular/core", "../../../services/hsd-es.service", "../../../models/hsdes-document.model", "../../../models/user.model", "../../../shared/helpers/hsdes-helper", "linq-es2015", "ng2-toastr/ng2-toastr", "ngx-bootstrap"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, hsd_es_service_1, hsdes_document_model_1, user_model_1, hsdes_helper_1, linq_es2015_1, ng2_toastr_1, ngx_bootstrap_1, QueryComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (hsd_es_service_1_1) {
                hsd_es_service_1 = hsd_es_service_1_1;
            },
            function (hsdes_document_model_1_1) {
                hsdes_document_model_1 = hsdes_document_model_1_1;
            },
            function (user_model_1_1) {
                user_model_1 = user_model_1_1;
            },
            function (hsdes_helper_1_1) {
                hsdes_helper_1 = hsdes_helper_1_1;
            },
            function (linq_es2015_1_1) {
                linq_es2015_1 = linq_es2015_1_1;
            },
            function (ng2_toastr_1_1) {
                ng2_toastr_1 = ng2_toastr_1_1;
            },
            function (ngx_bootstrap_1_1) {
                ngx_bootstrap_1 = ngx_bootstrap_1_1;
            }
        ],
        execute: function () {
            QueryComponent = (function () {
                function QueryComponent(hsdES, toastr, vcr) {
                    this.hsdES = hsdES;
                    this.toastr = toastr;
                    this.user = new user_model_1.User();
                    //ngTable Config Settings
                    this.page = 1;
                    this.itemsPerPage = 10;
                    this.pageSizes = [10, 25, 50, 100];
                    this.maxSize = 5;
                    this.numPages = 1;
                    this.length = 0;
                    this.filters = {
                        release: { filterString: '', placeholder: '' },
                        docName: { filterString: '', placeholder: '' },
                        domain: { filterString: '', placeholder: '' },
                        docType: { filterString: '', placeholder: '' },
                        owner: { filterString: '', placeholder: '' },
                        revision: { filterString: '', placeholder: '' },
                        status: { filterString: '', placeholder: '' },
                        authEta: { filterString: '', placeholder: '' },
                        reviewDate: { filterString: '', placeholder: '' },
                        releaseDate: { filterString: '', placeholder: '' }
                    };
                    this.config = {
                        paging: true,
                        className: ['table-striped', 'table-bordered']
                    };
                    this.toastr.setRootViewContainerRef(vcr);
                }
                QueryComponent.prototype.onChangeTable = function (page) {
                    var _this = this;
                    if (page === void 0) { page = { page: this.page, itemsPerPage: this.itemsPerPage }; }
                    var data = this.parents;
                    if (this.myDocsOnly) {
                        data = linq_es2015_1.asEnumerable(data).Where(function (p) { return p.owner.toUpperCase() == _this.user.idsid.toUpperCase(); }).ToArray();
                    }
                    var filteredData = this.changeFilter(data);
                    var sortedData = this.changeSort(filteredData);
                    this.rows = page && this.config.paging ? this.changePage(page, sortedData) : sortedData;
                    this.length = sortedData.length;
                };
                QueryComponent.prototype.changePage = function (page, data) {
                    if (data === void 0) { data = this.parents; }
                    var start = (page.page - 1) * page.itemsPerPage;
                    var end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
                    return data.slice(start, end);
                };
                QueryComponent.prototype.changeSort = function (data) {
                    if (!this.config.sorting) {
                        return data;
                    }
                    var columns = this.config.sorting.columns || [];
                    var columnName = void 0;
                    var sort = void 0;
                    for (var i = 0; i < columns.length; i++) {
                        if (columns[i].sort !== '') {
                            columnName = columns[i].name;
                            sort = columns[i].sort;
                        }
                    }
                    if (!columnName) {
                        return data;
                    }
                    // simple sorting
                    return data.sort(function (previous, current) {
                        if (previous[columnName] > current[columnName]) {
                            return sort === 'desc' ? -1 : 1;
                        }
                        else if (previous[columnName] < current[columnName]) {
                            return sort === 'asc' ? -1 : 1;
                        }
                        return 0;
                    });
                };
                QueryComponent.prototype.changeFilter = function (data) {
                    var _this = this;
                    var filteredData = data.filter(function (d) {
                        return d.release.toUpperCase().indexOf(_this.filters.release.filterString.toUpperCase()) >= 0
                            && d.docName.toUpperCase().indexOf(_this.filters.docName.filterString.toUpperCase()) >= 0
                            && d.domain.toUpperCase().indexOf(_this.filters.domain.filterString.toUpperCase()) >= 0
                            && d.docType.toUpperCase().indexOf(_this.filters.docType.filterString.toUpperCase()) >= 0
                            && d.owner.toUpperCase().indexOf(_this.filters.owner.filterString.toUpperCase()) >= 0
                            && d.selectedRevision.toUpperCase().indexOf(_this.filters.revision.filterString.toUpperCase()) >= 0
                            && (!d.selectedDoc || d.selectedDoc.status.toUpperCase().indexOf(_this.filters.status.filterString.toUpperCase()) >= 0)
                            && (!d.selectedDoc || d.selectedDoc.authoringEta.toUpperCase().indexOf(_this.filters.authEta.filterString.toUpperCase()) >= 0)
                            && (!d.selectedDoc || d.selectedDoc.reviewDate.toUpperCase().indexOf(_this.filters.reviewDate.filterString.toUpperCase()) >= 0)
                            && (!d.selectedDoc || d.selectedDoc.releaseEta.toUpperCase().indexOf(_this.filters.releaseDate.filterString.toUpperCase()) >= 0);
                    });
                    return filteredData;
                };
                QueryComponent.prototype.onChangeMyDocsOnly = function () {
                    if (this.parents && this.parents.length > 0)
                        this.onChangeTable();
                };
                QueryComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this.isModalShown = false;
                    this.queryReleaseAffected = false;
                    this.selectedDoc = new hsdes_document_model_1.HsdEsDocument();
                    this.selectedReleases = [];
                    this.showNewHasModal = false;
                    this.myDocsOnly = false;
                    this.revisions = hsdes_helper_1.HsdEsHelper.Revisions;
                    this.mySettings = {
                        enableSearch: true,
                        showCheckAll: true,
                        showUncheckAll: true,
                    };
                    this.hsdES.getReleases().then(function (response) {
                        _this.releases = response;
                    });
                    this.hsdES.getUser().then(function (response) {
                        _this.user = response;
                    });
                };
                QueryComponent.prototype.onSubmit = function () {
                    var _this = this;
                    this.hsdES.getDocs(this.selectedReleases, this.queryReleaseAffected).then(function (response) {
                        _this.docs = response;
                        _this.parents = hsdes_helper_1.HsdEsHelper.createHierarchy(_this.docs);
                        _this.onChangeTable();
                    });
                };
                QueryComponent.prototype.updateLinks = function (doc) {
                    this.selectedDoc = doc;
                    this.isModalShown = true;
                };
                QueryComponent.prototype.onClosed = function (close) {
                    this.isModalShown = close;
                    this.onSubmit();
                };
                QueryComponent.prototype.isValid = function () {
                    return this.selectedReleases && this.selectedReleases.length > 0;
                };
                QueryComponent.prototype.onChangeStatus = function (doc) {
                    if (doc.status != doc.currentStatus) {
                        this.selectedDoc = doc;
                        this.statusModal.show();
                    }
                };
                QueryComponent.prototype.showStatusModal = function () {
                    this.statusModal.show();
                };
                QueryComponent.prototype.createNewHas = function (doc) {
                    var _this = this;
                    this.selectedRelease = this.releases.find(function (r) { return r.name == _this.selectedReleases[0]; });
                    this.showNewHasModal = true;
                    this.newHasModal.show();
                };
                QueryComponent.prototype.cancel = function () {
                    this.showNewHasModal = false;
                    this.newHasModal.hide();
                };
                QueryComponent.prototype.editHas = function (parentDoc) {
                    this.selectedParent = parentDoc;
                    this.showEditModal = true;
                    this.editModal.show();
                };
                QueryComponent.prototype.cancelEditHas = function () {
                    this.showEditModal = false;
                    this.editModal.hide();
                };
                QueryComponent.prototype.rejectDoc = function (parentdoc) {
                    this.selectedParent = parentdoc;
                    this.rejectModal.show();
                };
                QueryComponent.prototype.parentDocIsSelected = function () {
                    if (this.selectedParent)
                        return true;
                    return false;
                };
                QueryComponent.prototype.refreshData = function () {
                    this.selectedParent = undefined;
                    this.rejectModal.hide();
                    this.onSubmit();
                };
                QueryComponent.prototype.revisionChange = function (parent) {
                    if (parent.requestedRevision > parent.currentRevision) {
                        //show modal to see if they want to waive
                        this.selectedParent = parent;
                        this.waiveModal.show();
                        return;
                    }
                    parent.selectedRevision = parent.requestedRevision;
                };
                QueryComponent.prototype.closedModal = function (updatedValues) {
                    //refactor to handle all modals? and close them
                    this.waiveModal.hide();
                    if (updatedValues)
                        this.onSubmit();
                };
                QueryComponent.prototype.hasResults = function () {
                    return this.parents && this.parents.length > 0;
                };
                return QueryComponent;
            }());
            __decorate([
                core_1.ViewChild('statusModal'),
                __metadata("design:type", ngx_bootstrap_1.ModalDirective)
            ], QueryComponent.prototype, "statusModal", void 0);
            __decorate([
                core_1.ViewChild('newHasModal'),
                __metadata("design:type", ngx_bootstrap_1.ModalDirective)
            ], QueryComponent.prototype, "newHasModal", void 0);
            __decorate([
                core_1.ViewChild('editModal'),
                __metadata("design:type", ngx_bootstrap_1.ModalDirective)
            ], QueryComponent.prototype, "editModal", void 0);
            __decorate([
                core_1.ViewChild('waiveModal'),
                __metadata("design:type", ngx_bootstrap_1.ModalDirective)
            ], QueryComponent.prototype, "waiveModal", void 0);
            __decorate([
                core_1.ViewChild('rejectModal'),
                __metadata("design:type", ngx_bootstrap_1.ModalDirective)
            ], QueryComponent.prototype, "rejectModal", void 0);
            QueryComponent = __decorate([
                core_1.Component({
                    selector: 'doc-query',
                    templateUrl: './query.component.html'
                }),
                __metadata("design:paramtypes", [hsd_es_service_1.HsdEsService, ng2_toastr_1.ToastsManager, core_1.ViewContainerRef])
            ], QueryComponent);
            exports_1("QueryComponent", QueryComponent);
        }
    };
});
//# sourceMappingURL=query.component.js.map