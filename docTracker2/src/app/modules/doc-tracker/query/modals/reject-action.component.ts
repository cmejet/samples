import { Component, OnInit, Input, Output, EventEmitter, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { HsdEsParentDoc } from '../../../../models/hsdes-parentdoc.model';
import { HsdEsService } from '../../../../services/hsd-es.service';


@Component({
    selector: 'reject-action',
    templateUrl: './reject-action.component.html'
})

export class RejectActionComponent implements OnInit {
    @Input() parentdoc: HsdEsParentDoc;
    @Output() onClosed = new EventEmitter<boolean>();
    public docName: string;
    constructor(private hsdES: HsdEsService, private vcr: ViewContainerRef, public toastr: ToastsManager) {
    }

    public reject(): void {
        this.hsdES.rejectDocumentParent(this.parentdoc)
            .then(() => {
                this.parentdoc.docs.forEach(doc => {
                    this.hsdES.rejectDocumentChild(doc);
                });
            })
            .then(() => {
                setTimeout(() => {
                    this.toastr.success('Document has been rejected');
                    this.onClosed.emit(false);
                }, 3000)
            });
    }
    public cancel(): void {
        this.toastr.info("Cancel Reject");
        this.onClosed.emit(false);
    }
    ngOnInit() {
        this.docName = this.parentdoc.docs[0].docName;
    }
}