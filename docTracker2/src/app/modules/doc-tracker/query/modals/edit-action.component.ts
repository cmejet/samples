import { Component, ViewContainerRef, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { OnInit } from '@angular/core';
import { HsdEsService } from '../../../../services/hsd-es.service';
import { HsdEsDocument } from "../../../../models/hsdes-document.model";
import { HsdEsParentDoc } from "../../../../models/hsdes-parentdoc.model";
import { HsdEsProduct } from "../../../../models/hsdes-product.model";
import { HsdEsHelper } from "../../../../shared/helpers/hsdes-helper";
import { HsdEsEditHelper } from "../../../../shared/helpers/hsdes-edit.helper";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
    selector: 'edit-action',
    templateUrl: './edit-action.component.html',
})

export class EditActionComponent implements OnInit {
    @Input() parentdoc: HsdEsParentDoc;
    @Output() onClosed = new EventEmitter<void>();

    constructor(private hsdES: HsdEsService, public toastr: ToastsManager, vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    public cancel(): void {
        this.toastr.info("Cancel Edit action");
        this.onClosed.emit();
    }

    public onSubmit(): void {

        this.hsdES.editActionParent(this.parentdoc).then(() => {
           //save all children after parent
            this.toastr.success("Form has been changed");
        });
    }
    ngOnInit() {

    }
}
