System.register(["@angular/core", "ng2-toastr/ng2-toastr", "../../../../models/hsdes-parentdoc.model", "../../../../services/hsd-es.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ng2_toastr_1, hsdes_parentdoc_model_1, hsd_es_service_1, RejectActionComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ng2_toastr_1_1) {
                ng2_toastr_1 = ng2_toastr_1_1;
            },
            function (hsdes_parentdoc_model_1_1) {
                hsdes_parentdoc_model_1 = hsdes_parentdoc_model_1_1;
            },
            function (hsd_es_service_1_1) {
                hsd_es_service_1 = hsd_es_service_1_1;
            }
        ],
        execute: function () {
            RejectActionComponent = (function () {
                function RejectActionComponent(hsdES, vcr, toastr) {
                    this.hsdES = hsdES;
                    this.vcr = vcr;
                    this.toastr = toastr;
                    this.onClosed = new core_1.EventEmitter();
                }
                RejectActionComponent.prototype.reject = function () {
                    var _this = this;
                    this.hsdES.rejectDocumentParent(this.parentdoc)
                        .then(function () {
                        _this.parentdoc.docs.forEach(function (doc) {
                            _this.hsdES.rejectDocumentChild(doc);
                        });
                    })
                        .then(function () {
                        setTimeout(function () {
                            _this.toastr.success('Document has been rejected');
                            _this.onClosed.emit(false);
                        }, 3000);
                    });
                };
                RejectActionComponent.prototype.cancel = function () {
                    this.toastr.info("Cancel Reject");
                    this.onClosed.emit(false);
                };
                RejectActionComponent.prototype.ngOnInit = function () {
                    this.docName = this.parentdoc.docs[0].docName;
                };
                return RejectActionComponent;
            }());
            __decorate([
                core_1.Input(),
                __metadata("design:type", hsdes_parentdoc_model_1.HsdEsParentDoc)
            ], RejectActionComponent.prototype, "parentdoc", void 0);
            __decorate([
                core_1.Output(),
                __metadata("design:type", Object)
            ], RejectActionComponent.prototype, "onClosed", void 0);
            RejectActionComponent = __decorate([
                core_1.Component({
                    selector: 'reject-action',
                    templateUrl: './reject-action.component.html'
                }),
                __metadata("design:paramtypes", [hsd_es_service_1.HsdEsService, core_1.ViewContainerRef, ng2_toastr_1.ToastsManager])
            ], RejectActionComponent);
            exports_1("RejectActionComponent", RejectActionComponent);
        }
    };
});
//# sourceMappingURL=reject-action.component.js.map