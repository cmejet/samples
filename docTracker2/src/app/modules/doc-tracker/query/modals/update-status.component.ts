import { Component, OnInit, Input, Output, EventEmitter, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { HsdEsDocument } from '../../../../models/hsdes-document.model';
import { HsdEsService } from '../../../../services/hsd-es.service';

@Component({
    selector: 'update-status',
    templateUrl: './update-status.component.html'
})

export class UpdateStatusComponent implements OnInit {
    @Input() doc: HsdEsDocument;
    @Output() onClosed = new EventEmitter<boolean>();

    constructor(private hsdES: HsdEsService, private vcr: ViewContainerRef, public toastr: ToastsManager) {
    }

    public updateStatus(): void {
        this.hsdES.updateDocStatus(this.doc).then(() => {
        this.toastr.success('Document has been saved');
        this.onClosed.emit(false);
        });
    }
    public cancel(): void {
        this.doc.status = this.doc.currentStatus;
        this.toastr.info("Cancel Update");
        this.onClosed.emit(false);
    }
    ngOnInit() { }
}