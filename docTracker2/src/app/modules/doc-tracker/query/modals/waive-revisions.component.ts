import { Component, OnInit, Input, Output, EventEmitter, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { HsdEsParentDoc } from '../../../../models/hsdes-parentdoc.model';
import { HsdEsService } from '../../../../services/hsd-es.service';
import { asEnumerable } from 'linq-es5';

@Component({
    selector: 'waive-revisions',
    templateUrl: './waive-revisions.component.html'
})

export class WaiveRevisionsComponent implements OnInit {
    @Input() parentdoc: HsdEsParentDoc;
    @Output() onClosed = new EventEmitter<boolean>();

    constructor(private hsdES: HsdEsService, private vcr: ViewContainerRef, public toastr: ToastsManager) {
    }

    public waive(): void {
        var promises = [];
        this.parentdoc.revsToWaive.forEach(d => {
            promises.push(this.hsdES.waiveDocument(d));
        })
        Promise.all(promises).then((responses) => {
            this.toastr.success("Revisions have been waived");
            this.onClosed.emit(true);
        });
    }
    public cancel(): void {
        this.parentdoc.requestedRevision = this.parentdoc.selectedRevision;
        this.onClosed.emit(false);
    }
    ngOnInit() { }
}