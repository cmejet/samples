import { Component, OnInit, Input, Output, EventEmitter, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { HsdEsDocument } from '../../../../models/hsdes-document.model';
import { HsdEsService } from '../../../../services/hsd-es.service';

@Component({
    selector: 'update-links',
    templateUrl: './update-links.component.html'
})

export class UpdateLinksComponent implements OnInit {
    @Input() doc: HsdEsDocument;
    @Output() onClosed = new EventEmitter<boolean>();

    constructor(private hsdES: HsdEsService, private vcr: ViewContainerRef, public toastr: ToastsManager) {
    }
    public updateLinks(): void {
        this.hsdES.updateDocUrls(this.doc).then(() => {
            this.toastr.success("Links Updated");
            this.onClosed.emit(false)
        });
    }
    public cancel(): void {
        this.toastr.info("Cancel Update");
        this.onClosed.emit(false);
    }

    ngOnInit() {

    }
}