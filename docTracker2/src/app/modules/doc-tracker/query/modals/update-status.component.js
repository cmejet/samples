System.register(["@angular/core", "ng2-toastr/ng2-toastr", "../../../../models/hsdes-document.model", "../../../../services/hsd-es.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, ng2_toastr_1, hsdes_document_model_1, hsd_es_service_1, UpdateStatusComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (ng2_toastr_1_1) {
                ng2_toastr_1 = ng2_toastr_1_1;
            },
            function (hsdes_document_model_1_1) {
                hsdes_document_model_1 = hsdes_document_model_1_1;
            },
            function (hsd_es_service_1_1) {
                hsd_es_service_1 = hsd_es_service_1_1;
            }
        ],
        execute: function () {
            UpdateStatusComponent = (function () {
                function UpdateStatusComponent(hsdES, vcr, toastr) {
                    this.hsdES = hsdES;
                    this.vcr = vcr;
                    this.toastr = toastr;
                    this.onClosed = new core_1.EventEmitter();
                }
                UpdateStatusComponent.prototype.updateStatus = function () {
                    var _this = this;
                    this.hsdES.updateDocStatus(this.doc).then(function () {
                        _this.toastr.success('Document has been saved');
                        _this.onClosed.emit(false);
                    });
                };
                UpdateStatusComponent.prototype.cancel = function () {
                    this.doc.status = this.doc.currentStatus;
                    this.toastr.info("Cancel Update");
                    this.onClosed.emit(false);
                };
                UpdateStatusComponent.prototype.ngOnInit = function () { };
                return UpdateStatusComponent;
            }());
            __decorate([
                core_1.Input(),
                __metadata("design:type", hsdes_document_model_1.HsdEsDocument)
            ], UpdateStatusComponent.prototype, "doc", void 0);
            __decorate([
                core_1.Output(),
                __metadata("design:type", Object)
            ], UpdateStatusComponent.prototype, "onClosed", void 0);
            UpdateStatusComponent = __decorate([
                core_1.Component({
                    selector: 'update-status',
                    templateUrl: './update-status.component.html'
                }),
                __metadata("design:paramtypes", [hsd_es_service_1.HsdEsService, core_1.ViewContainerRef, ng2_toastr_1.ToastsManager])
            ], UpdateStatusComponent);
            exports_1("UpdateStatusComponent", UpdateStatusComponent);
        }
    };
});
//# sourceMappingURL=update-status.component.js.map