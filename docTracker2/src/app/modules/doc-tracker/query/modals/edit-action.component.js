System.register(["@angular/core", "../../../../services/hsd-es.service", "../../../../models/hsdes-parentdoc.model", "ng2-toastr/ng2-toastr"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, hsd_es_service_1, hsdes_parentdoc_model_1, ng2_toastr_1, EditActionComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (hsd_es_service_1_1) {
                hsd_es_service_1 = hsd_es_service_1_1;
            },
            function (hsdes_parentdoc_model_1_1) {
                hsdes_parentdoc_model_1 = hsdes_parentdoc_model_1_1;
            },
            function (ng2_toastr_1_1) {
                ng2_toastr_1 = ng2_toastr_1_1;
            }
        ],
        execute: function () {
            EditActionComponent = (function () {
                function EditActionComponent(hsdES, toastr, vcr) {
                    this.hsdES = hsdES;
                    this.toastr = toastr;
                    this.onClosed = new core_1.EventEmitter();
                    this.toastr.setRootViewContainerRef(vcr);
                }
                EditActionComponent.prototype.cancel = function () {
                    this.toastr.info("Cancel Edit action");
                    this.onClosed.emit();
                };
                EditActionComponent.prototype.onSubmit = function () {
                    var _this = this;
                    this.hsdES.editActionParent(this.parentdoc).then(function () {
                        //save all children after parent
                        _this.toastr.success("Form has been changed");
                    });
                };
                EditActionComponent.prototype.ngOnInit = function () {
                };
                return EditActionComponent;
            }());
            __decorate([
                core_1.Input(),
                __metadata("design:type", hsdes_parentdoc_model_1.HsdEsParentDoc)
            ], EditActionComponent.prototype, "parentdoc", void 0);
            __decorate([
                core_1.Output(),
                __metadata("design:type", Object)
            ], EditActionComponent.prototype, "onClosed", void 0);
            EditActionComponent = __decorate([
                core_1.Component({
                    selector: 'edit-action',
                    templateUrl: './edit-action.component.html',
                }),
                __metadata("design:paramtypes", [hsd_es_service_1.HsdEsService, ng2_toastr_1.ToastsManager, core_1.ViewContainerRef])
            ], EditActionComponent);
            exports_1("EditActionComponent", EditActionComponent);
        }
    };
});
//# sourceMappingURL=edit-action.component.js.map