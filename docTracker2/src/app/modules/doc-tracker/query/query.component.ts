import { Component, OnInit, ViewContainerRef, ViewChild, EventEmitter } from '@angular/core';
import { HsdEsService } from '../../../services/hsd-es.service';
import { HsdEsDocument } from "../../../models/hsdes-document.model";
import { HsdEsParentDoc } from "../../../models/hsdes-parentdoc.model";
import { HsdEsProduct } from "../../../models/hsdes-product.model";
import { User } from "../../../models/user.model";
import { HsdEsHelper } from "../../../shared/helpers/hsdes-helper";
import { IMultiSelectSettings } from 'angular-2-dropdown-multiselect';
import { NG_TABLE_DIRECTIVES } from 'ng2-table/ng2-table';
import { asEnumerable } from 'linq-es5';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'doc-query',
    templateUrl: './query.component.html'
})
export class QueryComponent implements OnInit {

    @ViewChild('statusModal') public statusModal: ModalDirective;
    @ViewChild('newHasModal') public newHasModal: ModalDirective;
    @ViewChild('editModal') public editModal: ModalDirective;
    @ViewChild('waiveModal') public waiveModal: ModalDirective;
    @ViewChild('rejectModal') public rejectModal: ModalDirective;
    public user: User = new User();
    public releases: HsdEsProduct[];
    public selectedReleases: string[];
    public selectedRelease: HsdEsProduct;
    public selectedAction: string[];
    public revisions: string[];
    public docs: HsdEsDocument[];
    public selectedDoc: HsdEsDocument;
    public selectedParent: HsdEsParentDoc;
    public parents: HsdEsParentDoc[];
    public doc: any;
    public mySettings: IMultiSelectSettings;
    public queryReleaseAffected: boolean;
    public myDocsOnly: boolean;
    public isModalShown: boolean;
    public showNewHasModal: boolean;
    public showEditModal: boolean;

    public releaseAffected: string;
    public domainList: string[];
    public docTypeList: string[];

    //ngTable Config Settings
    public page: number = 1;
    public itemsPerPage: number = 10;
    public pageSizes:number[]=[10,25,50,100];
    public maxSize: number = 5;
    public numPages: number = 1;
    public length: number = 0;
    public filters: any = {
        release: { filterString: '', placeholder: '' },
        docName: { filterString: '', placeholder: '' },
        domain: { filterString: '', placeholder: '' },
        docType: { filterString: '', placeholder: '' },
        owner: { filterString: '', placeholder: '' },
        revision: { filterString: '', placeholder: '' },
        status: { filterString: '', placeholder: '' },
        authEta: { filterString: '', placeholder: '' },
        reviewDate: { filterString: '', placeholder: '' },
        releaseDate: { filterString: '', placeholder: '' }
    };
    public rows: any[];

    public config: any = {
        paging: true,
        className: ['table-striped', 'table-bordered']
    }

    constructor(private hsdES: HsdEsService, public toastr: ToastsManager, vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr);
    }
    public updateTable():void{
        this.onChangeTable();
    }
    public onChangeTable(page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {
        var data = this.parents;
        if (this.myDocsOnly) {
            data = asEnumerable(data).Where(p => p.owner.toUpperCase() == this.user.idsid.toUpperCase()).ToArray();
        }
        let filteredData = this.changeFilter(data);
        let sortedData = this.changeSort(filteredData);
        this.rows = page && this.config.paging ? this.changePage(page, sortedData) : sortedData;
        this.length = sortedData.length;
    }
    public changePage(page: any, data: Array<any> = this.parents): Array<any> {
        let start = (page.page - 1) * page.itemsPerPage;
        let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
        return data.slice(start, end);
    }

    public changeSort(data: any): any {
        if (!this.config.sorting) {
            return data;
        }

        let columns = this.config.sorting.columns || [];
        let columnName: string = void 0;
        let sort: string = void 0;

        for (let i = 0; i < columns.length; i++) {
            if (columns[i].sort !== '') {
                columnName = columns[i].name;
                sort = columns[i].sort;
            }
        }

        if (!columnName) {
            return data;
        }

        // simple sorting
        return data.sort((previous: any, current: any) => {
            if (previous[columnName] > current[columnName]) {
                return sort === 'desc' ? -1 : 1;
            } else if (previous[columnName] < current[columnName]) {
                return sort === 'asc' ? -1 : 1;
            }
            return 0;
        });
    }

    public changeFilter(data: any): any {
        var filteredData = data.filter(d =>
            d.release.toUpperCase().indexOf(this.filters.release.filterString.toUpperCase()) >= 0
            && d.docName.toUpperCase().indexOf(this.filters.docName.filterString.toUpperCase()) >= 0
            && d.domain.toUpperCase().indexOf(this.filters.domain.filterString.toUpperCase()) >= 0
            && d.docType.toUpperCase().indexOf(this.filters.docType.filterString.toUpperCase()) >= 0
            && d.owner.toUpperCase().indexOf(this.filters.owner.filterString.toUpperCase()) >= 0
            && d.selectedRevision.toUpperCase().indexOf(this.filters.revision.filterString.toUpperCase()) >= 0
            && (!d.selectedDoc || d.selectedDoc.status.toUpperCase().indexOf(this.filters.status.filterString.toUpperCase()) >= 0)
            && (!d.selectedDoc || d.selectedDoc.authoringEta.toUpperCase().indexOf(this.filters.authEta.filterString.toUpperCase()) >= 0)
            && (!d.selectedDoc || d.selectedDoc.reviewDate.toUpperCase().indexOf(this.filters.reviewDate.filterString.toUpperCase()) >= 0)
            && (!d.selectedDoc || d.selectedDoc.releaseEta.toUpperCase().indexOf(this.filters.releaseDate.filterString.toUpperCase()) >= 0)
        )
        return filteredData;
    }
    public onChangeMyDocsOnly(): void {
        if (this.parents && this.parents.length > 0)
            this.onChangeTable();
    }
    ngOnInit() {
        this.isModalShown = false;
        this.queryReleaseAffected = false;
        this.selectedDoc = new HsdEsDocument();
        this.selectedReleases = [];
        this.showNewHasModal = false;
        this.myDocsOnly = false;
        this.revisions = HsdEsHelper.Revisions;
        this.mySettings = {
            enableSearch: true,
            showCheckAll: true,
            showUncheckAll: true,
        };

        this.hsdES.getReleases().then(response => {
            this.releases = response;
        });
        this.hsdES.getUser().then(response => {
            this.user = response;
        });
    }

    public onSubmit(): void {
        this.hsdES.getDocs(this.selectedReleases, this.queryReleaseAffected).then(response => {
            this.docs = response;
            this.parents = HsdEsHelper.createHierarchy(this.docs);
            this.onChangeTable();
        });
    }
    public updateLinks(doc: HsdEsDocument): void {
        this.selectedDoc = doc;
        this.isModalShown = true;
    }
    public onClosed(close: boolean): void {
        this.isModalShown = close;
        this.onSubmit();
    }

    public isValid(): boolean {
        return this.selectedReleases && this.selectedReleases.length > 0;
    }

    public onChangeStatus(doc: HsdEsDocument): void {
        if (doc.status != doc.currentStatus) {
            this.selectedDoc = doc;
            this.statusModal.show();
        }
    }
    public showStatusModal(): void {
        this.statusModal.show();
    }

    public createNewHas(doc: HsdEsDocument): void {
        this.selectedRelease = this.releases.find(r => r.name == this.selectedReleases[0]);
        this.showNewHasModal = true;
        this.newHasModal.show();
    }

    public cancel(): void {
        this.showNewHasModal = false;
        this.newHasModal.hide();
    }

    public editHas(parentDoc: HsdEsParentDoc): void {
        this.selectedParent = parentDoc;
        this.showEditModal = true;
        this.editModal.show();
    }

    public cancelEditHas(): void {
        this.showEditModal = false;
        this.editModal.hide();
    }

    public rejectDoc(parentdoc: HsdEsParentDoc): void {
        this.selectedParent = parentdoc;
        this.rejectModal.show();
    }
    public parentDocIsSelected(): boolean {
        if (this.selectedParent)
            return true;
        return false;
    }
    public refreshData(): void {
        this.selectedParent = undefined;
        this.rejectModal.hide();
        this.onSubmit();
    }
    public revisionChange(parent: HsdEsParentDoc): void {
        if (parent.requestedRevision > parent.currentRevision) {
            //show modal to see if they want to waive
            this.selectedParent = parent;
            this.waiveModal.show();
            return;
        }
        parent.selectedRevision = parent.requestedRevision;
    }
    public closedModal(updatedValues: boolean) {
        //refactor to handle all modals? and close them
        this.waiveModal.hide();
        if (updatedValues)
            this.onSubmit();
    }
    public hasResults():boolean{
        return this.parents && this.parents.length > 0;
    }
}