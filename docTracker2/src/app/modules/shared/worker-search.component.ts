import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../../models/user.model';
import { WorkerService } from '../../services/worker.service';
@Component({
    selector: 'worker-search',
    templateUrl: './worker-search.component.html',
    styleUrls: ['./worker-search.scss']
})

export class WorkerSearchComponent implements OnInit {

    @Input() maxResults: number;
    @Input() idsid: string;
    @Output() onSelected = new EventEmitter<string>();
    constructor(private workerService: WorkerService) {
    }
    public workers: User[];
    public selectedWorker: User;
    public searchTerm: string;

    public searchWorkers(): void {
        if (this.searchTerm.length > 2) {
            setTimeout(() => {
                this.workerService.searchWorker(this.searchTerm).then(response => {
                    this.workers = response.slice(0, this.maxResults);
                });
            }, 1000);
        }
           }
    public selectWorker(worker: User): void {
        this.searchTerm = worker.fullName;
        this.workers = [];
        this.onSelected.emit(worker.idsid);
    }

    ngOnInit() {
        if(this.idsid && this.idsid.length)
            this.searchTerm = this.idsid;
    }
}