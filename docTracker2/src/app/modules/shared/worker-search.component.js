System.register(["@angular/core", "../../services/worker.service"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __moduleName = context_1 && context_1.id;
    var core_1, worker_service_1, WorkerSearchComponent;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (worker_service_1_1) {
                worker_service_1 = worker_service_1_1;
            }
        ],
        execute: function () {
            WorkerSearchComponent = (function () {
                function WorkerSearchComponent(workerService) {
                    this.workerService = workerService;
                    this.onSelected = new core_1.EventEmitter();
                }
                WorkerSearchComponent.prototype.searchWorkers = function () {
                    var _this = this;
                    if (this.searchTerm.length > 2) {
                        setTimeout(function () {
                            _this.workerService.searchWorker(_this.searchTerm).then(function (response) {
                                _this.workers = response.slice(0, _this.maxResults);
                            });
                        }, 1000);
                    }
                };
                WorkerSearchComponent.prototype.selectWorker = function (worker) {
                    this.searchTerm = worker.fullName;
                    this.workers = [];
                    this.onSelected.emit(worker.idsid);
                };
                WorkerSearchComponent.prototype.ngOnInit = function () {
                    if (this.idsid && this.idsid.length)
                        this.searchTerm = this.idsid;
                };
                return WorkerSearchComponent;
            }());
            __decorate([
                core_1.Input(),
                __metadata("design:type", Number)
            ], WorkerSearchComponent.prototype, "maxResults", void 0);
            __decorate([
                core_1.Input(),
                __metadata("design:type", String)
            ], WorkerSearchComponent.prototype, "idsid", void 0);
            __decorate([
                core_1.Output(),
                __metadata("design:type", Object)
            ], WorkerSearchComponent.prototype, "onSelected", void 0);
            WorkerSearchComponent = __decorate([
                core_1.Component({
                    selector: 'worker-search',
                    templateUrl: './worker-search.component.html',
                    styleUrls: ['../../../../styles/worker-search.scss']
                }),
                __metadata("design:paramtypes", [worker_service_1.WorkerService])
            ], WorkerSearchComponent);
            exports_1("WorkerSearchComponent", WorkerSearchComponent);
        }
    };
});
//# sourceMappingURL=worker-search.component.js.map