import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { DocTrackerComponent } from './modules/doc-tracker/doc-tracker.component';
import { QueryComponent } from './modules/doc-tracker/query/query.component';
import { SpecIndexComponent } from './modules/doc-tracker/spec-index/spec-index.component';
import { ImportSpecComponent } from './modules/doc-tracker/import/import-spec.component';
import { NewHasComponent } from './modules/doc-tracker/new-has/new-has.component';


const routes: Routes = [
 {
        path: 'doctracker',
        component: DocTrackerComponent
      },
      {
        path: 'specindex',
        component: SpecIndexComponent
      },
      {
        path: 'query',
        component: QueryComponent
      },
      {
        path: 'import',
        component: ImportSpecComponent
      },
      {
        path: 'newHas',
        component: NewHasComponent
      }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
